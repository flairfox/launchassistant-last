#ifndef ORBIT_H
#define ORBIT_H

#include <QStringList>
#include <QString>
#include <QTime>

class Orbit {

public:

// Конструкторы

    Orbit();
    Orbit(QString orbit_name);
    Orbit(QString orbit_name, QString scraft_name);

// Признак отделения

    /* Если равен 0 - активный участок
     * Если равен 1 - отделение */

    short type_flag;

// Имя орбиты и объекта

    QString orbit_name,
            scraft_name;

// Время оскуляции

    double t_tmi,
           t_ref;

// Оскулирующий период орбиты

    double period_tmi,
           period_ref,
           period_tol,
           period_dif;

// Элементы орбиты

/* [0] = a      большая полуось, км
 * [1] = e      эксцентриситет
 * [2] = hp     высота перигея, км
 * [3] = ha     высота апогея, км
 * [4] = i      наклонение, град
 * [5] = node   долгота восходящего узла, град
 * [6] = w      аргумент перигея, град
 * [7] = u      аргумет широты, град
 * [8] = teta   истинная аномалия, град */

    double elem_tmi[9],
           elem_ref[9],
           elem_tol[9],
           elem_dif[9];

// Флаг нахождения параметра в допуске

    QStringList tol_flag;

// Необходимые вектора

    double vect_tmi[6],
           vect_ref[6],
           vect_calc[6];

// Флаг исходного вектора

    /* Если равен 0 - АСН
     * Если равен 1 - бортовая аппаратура */

    short calc_flag;

// Методы

    void calc_dif();

};

#endif // ORBIT_H
