#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "loadwindow.h"
#include "settingswindow.h"
#include "resultwindow.h"
#include "orbit.h"

namespace Ui {

class MainWindow;

}

class MainWindow : public QMainWindow {

    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

public slots:

    void recieve_43_vect(double *vect);
    void recieve_100_vect(double *vect);

    void reload_list();

private slots:

    void on_button_open_43_clicked();
    void on_button_open_100_clicked();

    void on_cbox_orbit_43_currentIndexChanged(int index);
    void on_cbox_orbit_100_currentIndexChanged(int index);

    void on_button_calc_43_clicked();

    void ledit_43_changed();
    void ledit_100_changed();

    void on_action_quit_triggered();

    void on_action_open_triggered();

    void on_button_calc_100_clicked();

signals:

    void send_43_request(QString fname, double current_time);
    void send_100_request(QString fname, double current_time);

    void send_orbit(Orbit orbit);
    void result_clear();
    void result_show();

private:

    Ui::MainWindow *ui;

    LoadWindow *load_window;
    SettingsWindow *settings_window;
    ResultWindow *result_window;

    void fill_cbox();

    void load_orbit(int count);
};

#endif // MAINWINDOW_H
