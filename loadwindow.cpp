#include "loadwindow.h"
#include "ui_loadwindow.h"

#include <QTextStream>
#include <QString>
#include <QVector>
#include <QTime>
#include <QFile>

LoadWindow::LoadWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoadWindow) {

    ui->setupUi(this);

    this->setWindowFlags(Qt::WindowCloseButtonHint);
    ui->tree_widget->header()->setSectionResizeMode(QHeaderView::Fixed);
    connect(ui->tree_widget, SIGNAL(doubleClicked(QModelIndex)), ui->button_ok, SLOT(click()));
}

LoadWindow::~LoadWindow() {

    delete ui;
}

int flag;

void LoadWindow::recieve_43_request(QString fname, double current_time) {

    QStringList tmi, tmi_line;
    QString tmi_elem;
    QTreeWidgetItem *widget_item;
    double vect[7];
    double prev_time;
    int k = 0;

    flag = 43;

    for (int i = 0; i < ui->tree_widget->topLevelItemCount()+1; i++) {
        delete ui->tree_widget->topLevelItem(i);
    }
    ui->tree_widget->clear();

    QFile tmi_file(fname); tmi_file.open(QIODevice::ReadOnly|QIODevice::Text);
    QTextStream stream_tmi(&tmi_file);

    while(!stream_tmi.atEnd()) {
        tmi.append(stream_tmi.readLine());
    }

    tmi_file.close();

    for (int i = 5; i < tmi.count(); i++) {

        tmi_line = tmi.at(i).split("\t", QString::SkipEmptyParts);
        if (tmi_line.count() < 11) continue;

        //Бортовое время

        tmi_elem = tmi_line.at(1);
        tmi_elem.replace(",", "."); tmi_elem.simplified();
        vect[0] = tmi_elem.toDouble();

        if (vect[0] == prev_time) continue;
        if (vect[0] < current_time) continue;
        if (vect[0] >= current_time) k++;
        if (k > 15) break;

        prev_time = vect[0];

        //Скорость

        for (int j = 3; j < 6; j++) {
            tmi_elem = tmi_line.at(j + 2);
            tmi_elem.replace(",", "."); tmi_elem.simplified();
            vect[j+1] = tmi_elem.toDouble();
        }

        //Вектор
        for (int j = 0; j < 3; j++) {
            tmi_elem = tmi_line.at(j + 8);
            tmi_elem.replace(",", "."); tmi_elem.simplified();
            vect[j+1] = tmi_elem.toDouble();
        }

        //Записываем в вектор и выводим на экран

        widget_item = new QTreeWidgetItem (ui->tree_widget);
        widget_item->setText(0, QString::number(vect[0], 'f', 3));
        widget_item->setText(1, QString::number(vect[1], 'f', 3));
        widget_item->setText(2, QString::number(vect[2], 'f', 3));
        widget_item->setText(3, QString::number(vect[3], 'f', 3));
        widget_item->setText(4, QString::number(vect[4], 'f', 3));
        widget_item->setText(5, QString::number(vect[5], 'f', 3));
        widget_item->setText(6, QString::number(vect[6], 'f', 3));
    }
    ui->tree_widget->setCurrentItem(ui->tree_widget->topLevelItem(0));
}

void LoadWindow::recieve_100_request(QString fname, double current_time) {

    QStringList tmi, tmi_line;
    QString tmi_elem, asn_code;
    QTreeWidgetItem *widget_item;
    double vect[7];
    double prev_time;
    int k = 0;

    flag = 100;

    for (int i = 0; i < ui->tree_widget->topLevelItemCount()+1; i++) {
        delete ui->tree_widget->topLevelItem(i);
    }
    ui->tree_widget->clear();

    QFile tmi_file(fname); tmi_file.open(QIODevice::ReadOnly|QIODevice::Text);
    QTextStream stream_tmi(&tmi_file);

    while(!stream_tmi.atEnd()) {
        tmi.append(stream_tmi.readLine());
    }

    tmi_file.close();

    for (int i = 5; i < tmi.count(); i++) {

        tmi_line = tmi.at(i).split("\t", QString::SkipEmptyParts);
        if (tmi_line.count() < 11) continue;

        asn_code = tmi_line.at(6).mid(2, 6);
        //if (asn_code != "03117F") continue;

        //Бортовое время

        tmi_elem = tmi_line.at(1);
        tmi_elem.replace(",", "."); tmi_elem.simplified();
        vect[0] = tmi_elem.toDouble();

        if (vect[0] == prev_time) continue;
        if (vect[0] < current_time) continue;
        if (vect[0] >= current_time) k++;
        if (k > 15) break;

        prev_time = vect[0];

        //Вектор и скорость

        for (int j = 1; j < 7; j++) {
            tmi_elem = tmi_line.at(j + 6);
            tmi_elem.replace(",", "."); tmi_elem.simplified();
            vect[j] = tmi_elem.toDouble();
        }

        //Записываем в вектор и выводим на экран

        widget_item = new QTreeWidgetItem (ui->tree_widget);
        widget_item->setText(0, QString::number(vect[0], 'f', 3));
        widget_item->setText(1, QString::number(vect[1], 'f', 3));
        widget_item->setText(2, QString::number(vect[2], 'f', 3));
        widget_item->setText(3, QString::number(vect[3], 'f', 3));
        widget_item->setText(4, QString::number(vect[4], 'f', 3));
        widget_item->setText(5, QString::number(vect[5], 'f', 3));
        widget_item->setText(6, QString::number(vect[6], 'f', 3));
    }
    ui->tree_widget->setCurrentItem(ui->tree_widget->topLevelItem(0));
}

void LoadWindow::on_button_ok_clicked() {

    double vect[7];

    if (ui->tree_widget->selectedItems().size() == 0) return;
    for (int i = 0; i < 7; i++) {
        vect[i] = ui->tree_widget->currentItem()->text(i).toDouble();
    }

    if (flag == 43) emit send_43_vect(vect);
    if (flag == 100) emit send_100_vect(vect);

    this->close();
}

void LoadWindow::on_button_cancel_clicked() {

    this->close();
}
