#include "editwindow.h"
#include "ui_editwindow.h"
#include "conversions.h"

#include "orbit.h"

#include <QTime>

EditWindow::EditWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditWindow) {

    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowCloseButtonHint);
}

EditWindow::~EditWindow() {

    delete ui;
}

void EditWindow::recieve_orbit(Orbit orbit){

    flag = 1;

    this->setWindowTitle("Редактирование параметров орбиты: " + orbit.orbit_name);

    ui->ledit_orbit_name->setText(orbit.orbit_name);
    ui->ledit_scraft_name->setText(orbit.scraft_name);
    ui->tedit_period->setTime(SecondsToTime(orbit.period_ref));
    ui->ledit_t->setText   (QString::number(orbit.t_ref,       'f', 3));
    ui->ledit_a->setText   (QString::number(orbit.elem_ref[0], 'f', 1));
    ui->ledit_e->setText   (QString::number(orbit.elem_ref[1], 'f', 5));
    ui->ledit_hp->setText  (QString::number(orbit.elem_ref[2], 'f', 1));
    ui->ledit_ha->setText  (QString::number(orbit.elem_ref[3], 'f', 1));
    ui->ledit_i->setText   (QString::number(orbit.elem_ref[4], 'f', 2));
    ui->ledit_node->setText(QString::number(orbit.elem_ref[5], 'f', 2));
    ui->ledit_w->setText   (QString::number(orbit.elem_ref[6], 'f', 2));
    ui->ledit_u->setText   (QString::number(orbit.elem_ref[7], 'f', 2));
    ui->ledit_teta->setText(QString::number(orbit.elem_ref[8], 'f', 2));

    ui->ledit_period_tol->setText(QString::number(orbit.period_tol, 'f', 1));
    ui->ledit_a_tol->setText    (QString::number(orbit.elem_tol[0], 'f', 1));
    ui->ledit_e_tol->setText    (QString::number(orbit.elem_tol[1], 'f', 5));
    ui->ledit_hp_tol->setText   (QString::number(orbit.elem_tol[2], 'f', 1));
    ui->ledit_ha_tol->setText   (QString::number(orbit.elem_tol[3], 'f', 1));
    ui->ledit_i_tol->setText    (QString::number(orbit.elem_tol[4], 'f', 2));
    ui->ledit_node_tol->setText (QString::number(orbit.elem_tol[5], 'f', 2));
    ui->ledit_w_tol->setText    (QString::number(orbit.elem_tol[6], 'f', 2));
    ui->ledit_u_tol->setText    (QString::number(orbit.elem_tol[7], 'f', 2));
    ui->ledit_teta_tol->setText (QString::number(orbit.elem_tol[8], 'f', 2));
}

void EditWindow::recieve_request() {

    flag = 0;

    this->setWindowTitle("Добавление новой орбиты");

    ui->ledit_orbit_name->setText   ("");
    ui->ledit_scraft_name->setText   ("");
    ui->ledit_t->setText      ("");
    ui->tedit_period->setTime (QTime::fromString("00:00:00.000", "hh:mm:ss.zzz"));
    ui->ledit_a->setText      ("");
    ui->ledit_e->setText      ("");
    ui->ledit_hp->setText     ("");
    ui->ledit_ha->setText     ("");
    ui->ledit_i->setText      ("");
    ui->ledit_node->setText   ("");
    ui->ledit_w->setText      ("");
    ui->ledit_u->setText      ("");
    ui->ledit_teta->setText   ("");

    ui->ledit_period_tol->setText("");
    ui->ledit_a_tol->setText     ("");
    ui->ledit_e_tol->setText     ("");
    ui->ledit_hp_tol->setText    ("");
    ui->ledit_ha_tol->setText    ("");
    ui->ledit_i_tol->setText     ("");
    ui->ledit_node_tol->setText  ("");
    ui->ledit_w_tol->setText     ("");
    ui->ledit_u_tol->setText     ("");
    ui->ledit_teta_tol->setText  ("");
}

void EditWindow::on_button_ok_clicked() {

    Orbit orbit;

    orbit.orbit_name = ui->ledit_orbit_name->text();
    orbit.scraft_name = ui->ledit_scraft_name->text();

    orbit.period_ref = TimeToSeconds(ui->tedit_period->time());
    orbit.t_ref =       ui->ledit_t->text().toDouble();
    orbit.elem_ref[0] = ui->ledit_a->text().toDouble();
    orbit.elem_ref[1] = ui->ledit_e->text().toDouble();
    orbit.elem_ref[2] = ui->ledit_hp->text().toDouble();
    orbit.elem_ref[3] = ui->ledit_ha->text().toDouble();
    orbit.elem_ref[4] = ui->ledit_i->text().toDouble();
    orbit.elem_ref[5] = ui->ledit_node->text().toDouble();
    orbit.elem_ref[6] = ui->ledit_w->text().toDouble();
    orbit.elem_ref[7] = ui->ledit_u->text().toDouble();
    orbit.elem_ref[8] = ui->ledit_teta->text().toDouble();

    orbit.period_tol =  ui->ledit_period_tol->text().toDouble();
    orbit.elem_tol[0] = ui->ledit_a_tol->text().toDouble();
    orbit.elem_tol[1] = ui->ledit_e_tol->text().toDouble();
    orbit.elem_tol[2] = ui->ledit_hp_tol->text().toDouble();
    orbit.elem_tol[3] = ui->ledit_ha_tol->text().toDouble();
    orbit.elem_tol[4] = ui->ledit_i_tol->text().toDouble();
    orbit.elem_tol[5] = ui->ledit_node_tol->text().toDouble();
    orbit.elem_tol[6] = ui->ledit_w_tol->text().toDouble();
    orbit.elem_tol[7] = ui->ledit_u_tol->text().toDouble();
    orbit.elem_tol[8] = ui->ledit_teta_tol->text().toDouble();

    if (flag == 1) emit send_edited(orbit);
    if (flag == 0) emit send_new(orbit);

    this->close();
}

void EditWindow::on_button_cancel_clicked() {

    ui->ledit_orbit_name->setText   ("");
    ui->ledit_scraft_name->setText   ("");
    ui->ledit_t->setText      ("");
    ui->tedit_period->setTime (QTime::fromString("00:00:00.000", "hh:mm:ss.zzz"));
    ui->ledit_a->setText      ("");
    ui->ledit_e->setText      ("");
    ui->ledit_hp->setText     ("");
    ui->ledit_ha->setText     ("");
    ui->ledit_i->setText      ("");
    ui->ledit_node->setText   ("");
    ui->ledit_w->setText      ("");
    ui->ledit_u->setText      ("");
    ui->ledit_teta->setText   ("");

    ui->ledit_period_tol->setText("");
    ui->ledit_a_tol->setText     ("");
    ui->ledit_e_tol->setText     ("");
    ui->ledit_hp_tol->setText    ("");
    ui->ledit_ha_tol->setText    ("");
    ui->ledit_i_tol->setText     ("");
    ui->ledit_node_tol->setText  ("");
    ui->ledit_w_tol->setText     ("");
    ui->ledit_u_tol->setText     ("");
    ui->ledit_teta_tol->setText  ("");

    this->close();
}
