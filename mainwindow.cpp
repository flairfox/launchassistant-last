#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "loadwindow.h"
#include "resultwindow.h"
#include "functions.h"
#include "conversions.h"
#include "constants.h"
#include "orbit.h"
#include "math.h"
#include "RK8CC.h"

#include <QCoreApplication>
#include <QMessageBox>
#include <QFileDialog>
#include <QTextStream>
#include <QSettings>
#include <QVector>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {

    ui->setupUi(this);

    ui->button_calc_43->setEnabled(false);
    ui->button_calc_100->setEnabled(false);

    //Создаем экземпляры всех необходимых окон

    load_window = new LoadWindow();
    settings_window = new SettingsWindow();
    result_window = new ResultWindow();

    //Заполняем cbox списком орбит

    fill_cbox();
    load_orbit(0);

    //Подключаем сигналы, необходимые для загрузки ФО43

    connect(this, SIGNAL(send_43_request(QString, double)), load_window, SLOT(recieve_43_request(QString, double)));
    connect(this, SIGNAL(send_43_request(QString, double)), load_window, SLOT(show()));
    connect(load_window, SIGNAL(send_43_vect(double*)), this, SLOT(recieve_43_vect(double*)));

    //Подключаем сигналы, необходимые для загрузки ФО100

    connect(this, SIGNAL(send_100_request(QString, double)), load_window, SLOT(recieve_100_request(QString, double)));
    connect(this, SIGNAL(send_100_request(QString, double)), load_window, SLOT(show()));
    connect(load_window, SIGNAL(send_100_vect(double*)), this, SLOT(recieve_100_vect(double*)));

    //Подключаем сигналы для открытия окна настроек

    connect(ui->button_settings_100, SIGNAL(clicked()), ui->button_settings_43, SLOT(click()));
    connect(ui->button_settings_43, SIGNAL(clicked()), settings_window, SLOT(refresh_settings()));
    connect(ui->button_settings_43, SIGNAL(clicked()), settings_window, SLOT(show()));
    connect(settings_window, SIGNAL(reload_list()), this, SLOT(reload_list()));

    //Поключаем слоты и сигналы для вывода результатов в окно resultwindow

    connect(this, SIGNAL(send_orbit(Orbit)), result_window, SLOT(recieve_orbit(Orbit)));
    connect(this, SIGNAL(result_clear()), result_window, SLOT(result_clear()));
    connect(this, SIGNAL(result_show()), result_window, SLOT(result_show()));
    connect(this, SIGNAL(result_show()), result_window, SLOT(show()));

    //Подключаем сигналы изменения полей ввода вектора

    connect(ui->ledit_tTMI_100, SIGNAL(textChanged(QString)), this, SLOT(ledit_100_changed()));
    connect(ui->ledit_Rx_100,   SIGNAL(textChanged(QString)), this, SLOT(ledit_100_changed()));
    connect(ui->ledit_Ry_100,   SIGNAL(textChanged(QString)), this, SLOT(ledit_100_changed()));
    connect(ui->ledit_Rz_100,   SIGNAL(textChanged(QString)), this, SLOT(ledit_100_changed()));
    connect(ui->ledit_Vx_100,   SIGNAL(textChanged(QString)), this, SLOT(ledit_100_changed()));
    connect(ui->ledit_Vy_100,   SIGNAL(textChanged(QString)), this, SLOT(ledit_100_changed()));
    connect(ui->ledit_Vz_100,   SIGNAL(textChanged(QString)), this, SLOT(ledit_100_changed()));

    connect(ui->ledit_tTMI_43, SIGNAL(textChanged(QString)), this, SLOT(ledit_43_changed()));
    connect(ui->ledit_Rx_43,   SIGNAL(textChanged(QString)), this, SLOT(ledit_43_changed()));
    connect(ui->ledit_Ry_43,   SIGNAL(textChanged(QString)), this, SLOT(ledit_43_changed()));
    connect(ui->ledit_Rz_43,   SIGNAL(textChanged(QString)), this, SLOT(ledit_43_changed()));
    connect(ui->ledit_Vx_43,   SIGNAL(textChanged(QString)), this, SLOT(ledit_43_changed()));
    connect(ui->ledit_Vy_43,   SIGNAL(textChanged(QString)), this, SLOT(ledit_43_changed()));
    connect(ui->ledit_Vz_43,   SIGNAL(textChanged(QString)), this, SLOT(ledit_43_changed()));

    connect(ui->action_settings, SIGNAL(triggered()), ui->button_settings_43, SLOT(click()));
}

MainWindow::~MainWindow() {

    delete ui;
}

Orbit current_orbit;
double t_load;

void MainWindow::on_button_open_100_clicked() {

    //Обрабатываем кнопку загрузки ФО100

    QString fname, fcheck;

    fname = QFileDialog::getOpenFileName(this, "Открыть файл, содежащий обобщённый формуляр 100", "", "ФО100 (*.txt);;Все файлы (*)");
    if (fname == "")
        return;

    QFile tmi_file(fname); tmi_file.open(QIODevice::ReadOnly|QIODevice::Text);
    QTextStream stream_tmi(&tmi_file);

    fcheck = stream_tmi.readLine();

    fcheck = fcheck.mid(0, 14);

    if (fcheck != "Формуляр ФО100") {
        QMessageBox::critical(this, "Ошибка!", "Выбран неверный формуляр!", QMessageBox::Ok);
        return;
    }

    tmi_file.close();

    emit send_100_request(fname, t_load);
}

void MainWindow::recieve_100_vect(double *vect) {

    ui->ledit_tTMI_100->setText (QString::number(vect[0], 'f', 3));
    ui->ledit_Rx_100->setText   (QString::number(vect[1], 'f', 3));
    ui->ledit_Ry_100->setText   (QString::number(vect[2], 'f', 3));
    ui->ledit_Rz_100->setText   (QString::number(vect[3], 'f', 3));
    ui->ledit_Vx_100->setText   (QString::number(vect[4], 'f', 3));
    ui->ledit_Vy_100->setText   (QString::number(vect[5], 'f', 3));
    ui->ledit_Vz_100->setText   (QString::number(vect[6], 'f', 3));
}

void MainWindow::on_button_open_43_clicked() {

    //Обрабатываем кнопку загрузки ФО43

    QString fname, fcheck;

    fname = QFileDialog::getOpenFileName(this, "Открыть файл, содежащий обобщённый формуляр 43", "", "ФО43 (*.txt);;Все файлы (*)");
    if (fname == "")
        return;

    QFile tmi_file(fname); tmi_file.open(QIODevice::ReadOnly|QIODevice::Text);
    QTextStream stream_tmi(&tmi_file);

    fcheck = stream_tmi.readLine();

    fcheck = fcheck.mid(0, 13);

    if (fcheck != "Формуляр ФО43") {
        QMessageBox::critical(this, "Ошибка!", "Выбран неверный формуляр!", QMessageBox::Ok);
        return;
    }

    tmi_file.close();

    emit send_43_request(fname, t_load);
}

void MainWindow::recieve_43_vect(double *vect) {

    ui->ledit_tTMI_43->setText(QString::number(vect[0], 'f', 3));
    ui->ledit_Rx_43->setText  (QString::number(vect[1], 'f', 3));
    ui->ledit_Ry_43->setText  (QString::number(vect[2], 'f', 3));
    ui->ledit_Rz_43->setText  (QString::number(vect[3], 'f', 3));
    ui->ledit_Vx_43->setText  (QString::number(vect[4], 'f', 3));
    ui->ledit_Vy_43->setText  (QString::number(vect[5], 'f', 3));
    ui->ledit_Vz_43->setText  (QString::number(vect[6], 'f', 3));
}

void MainWindow::fill_cbox() {

    QSettings orbit_list("settings/orbitlist.ini", QSettings::IniFormat);

    int n = orbit_list.value("0/n").toInt();

    ui->cbox_orbit_43->clear();
    ui->cbox_orbit_100->clear();

    for (int i = 1; i < n+1; i++){

        ui->cbox_orbit_43->addItem(orbit_list.value(QString::number(i) + "/orbit_name").toString());
        ui->cbox_orbit_100->addItem(orbit_list.value(QString::number(i) + "/orbit_name").toString());
    }
}

void MainWindow::reload_list() {

    fill_cbox();
    load_orbit(0);
}

void MainWindow::load_orbit(int count) {

    //Загрузка орбиты с параметрами из "orbitlist.ini"

    QSettings orbit_list("settings/orbitlist.ini", QSettings::IniFormat);
    QString orbit_count = QString::number(count + 1);

    Orbit orbit;
    orbit.orbit_name =  orbit_list.value(orbit_count + "/orbit_name").toString();
    orbit.period_ref =  orbit_list.value(orbit_count + "/period").toDouble();
    orbit.t_ref =       orbit_list.value(orbit_count + "/t").toDouble();
    orbit.elem_ref[0] = orbit_list.value(orbit_count + "/a").toDouble();
    orbit.elem_ref[1] = orbit_list.value(orbit_count + "/e").toDouble();
    orbit.elem_ref[2] = orbit_list.value(orbit_count + "/hp").toDouble();
    orbit.elem_ref[3] = orbit_list.value(orbit_count + "/ha").toDouble();
    orbit.elem_ref[4] = orbit_list.value(orbit_count + "/i").toDouble();
    orbit.elem_ref[5] = orbit_list.value(orbit_count + "/node").toDouble();
    orbit.elem_ref[6] = orbit_list.value(orbit_count + "/w").toDouble();
    orbit.elem_ref[7] = orbit_list.value(orbit_count + "/u").toDouble();
    orbit.elem_ref[8] = orbit_list.value(orbit_count + "/teta").toDouble();

    t_load = orbit.t_ref;
    current_orbit = orbit;

    ui->ledit_tREF_43->setText(QString::number(orbit.t_ref, 'f',3));
    ui->ledit_tREF_100->setText(QString::number(orbit.t_ref,'f',3));

    ui->tedit_period_43->setTime(SecondsToTime(orbit.period_ref));
    ui->tedit_period_100->setTime(SecondsToTime(orbit.period_ref));

    ui->ledit_a_43->setText(QString::number(orbit.elem_ref[0], 'f',1));
    ui->ledit_a_100->setText(QString::number(orbit.elem_ref[0],'f',1));

    ui->ledit_e_43->setText(QString::number(orbit.elem_ref[1], 'f',5));
    ui->ledit_e_100->setText(QString::number(orbit.elem_ref[1],'f',5));

    ui->ledit_hp_43->setText(QString::number(orbit.elem_ref[2], 'f',1));
    ui->ledit_hp_100->setText(QString::number(orbit.elem_ref[2],'f',1));

    ui->ledit_ha_43->setText(QString::number(orbit.elem_ref[3], 'f',1));
    ui->ledit_ha_100->setText(QString::number(orbit.elem_ref[3],'f',1));

    ui->ledit_i_43->setText(QString::number(orbit.elem_ref[4], 'f',2));
    ui->ledit_i_100->setText(QString::number(orbit.elem_ref[4],'f',2));

    ui->ledit_node_43->setText(QString::number(orbit.elem_ref[5], 'f',2));
    ui->ledit_node_100->setText(QString::number(orbit.elem_ref[5],'f',2));

    ui->ledit_w_43->setText(QString::number(orbit.elem_ref[6], 'f',2));
    ui->ledit_w_100->setText(QString::number(orbit.elem_ref[6],'f',2));

    ui->ledit_u_43->setText(QString::number(orbit.elem_ref[7], 'f',2));
    ui->ledit_u_100->setText(QString::number(orbit.elem_ref[7],'f',2));

    ui->ledit_teta_43->setText(QString::number(orbit.elem_ref[8], 'f',2));
    ui->ledit_teta_100->setText(QString::number(orbit.elem_ref[8],'f',2));
}

void MainWindow::on_cbox_orbit_100_currentIndexChanged(int index) {

    load_orbit(index);
    ui->cbox_orbit_43->setCurrentIndex(index);

    ui->ledit_tTMI_100->setText("");
    ui->ledit_Rx_100->setText("");
    ui->ledit_Ry_100->setText("");
    ui->ledit_Rz_100->setText("");
    ui->ledit_Vx_100->setText("");
    ui->ledit_Vy_100->setText("");
    ui->ledit_Vz_100->setText("");
}

void MainWindow::on_cbox_orbit_43_currentIndexChanged(int index) {

    load_orbit(index);
    ui->cbox_orbit_100->setCurrentIndex(index);

    ui->ledit_tTMI_43->setText("");
    ui->ledit_Rx_43->setText("");
    ui->ledit_Ry_43->setText("");
    ui->ledit_Rz_43->setText("");
    ui->ledit_Vx_43->setText("");
    ui->ledit_Vy_43->setText("");
    ui->ledit_Vz_43->setText("");
}

void MainWindow::on_button_calc_43_clicked() {

    QString orbit_count;
    Orbit orbit;
    double rV[6];

    QSettings orbit_list("settings/orbitlist.ini", QSettings::IniFormat);
    orbit_count = QString::number(ui->cbox_orbit_43->currentIndex() + 1);

    orbit.orbit_name = orbit_list.value(orbit_count + "/orbit_name").toString();
    orbit.scraft_name = orbit_list.value(orbit_count + "/scraft_name").toString();

    orbit.t_tmi = ui->ledit_tTMI_43->text().toDouble();
    rV[0] = ui->ledit_Rx_43->text().toDouble();
    rV[1] = ui->ledit_Ry_43->text().toDouble();
    rV[2] = ui->ledit_Rz_43->text().toDouble();
    rV[3] = ui->ledit_Vx_43->text().toDouble();
    rV[4] = ui->ledit_Vy_43->text().toDouble();
    rV[5] = ui->ledit_Vz_43->text().toDouble();

    orbit.period_ref = TimeToSeconds(ui->tedit_period_43->time());
    orbit.t_ref = ui->ledit_tREF_43->text().toDouble();
    orbit.elem_ref[0] =    ui->ledit_a_43->text().toDouble();
    orbit.elem_ref[1] =    ui->ledit_e_43->text().toDouble();
    orbit.elem_ref[2] =   ui->ledit_hp_43->text().toDouble();
    orbit.elem_ref[3] =   ui->ledit_ha_43->text().toDouble();
    orbit.elem_ref[4] =    ui->ledit_i_43->text().toDouble()*dtr;
    orbit.elem_ref[5] = ui->ledit_node_43->text().toDouble()*dtr;
    orbit.elem_ref[6] =    ui->ledit_w_43->text().toDouble()*dtr;
    orbit.elem_ref[7] =    ui->ledit_u_43->text().toDouble()*dtr;
    orbit.elem_ref[8] = ui->ledit_teta_43->text().toDouble()*dtr;

    orbit.period_tol = orbit_list.value(orbit_count + "_tol/period").toDouble();
    orbit.elem_tol[0] =     orbit_list.value(orbit_count + "_tol/a").toDouble();
    orbit.elem_tol[1] =     orbit_list.value(orbit_count + "_tol/e").toDouble();
    orbit.elem_tol[2] =    orbit_list.value(orbit_count + "_tol/hp").toDouble();
    orbit.elem_tol[3] =    orbit_list.value(orbit_count + "_tol/ha").toDouble();
    orbit.elem_tol[4] =     orbit_list.value(orbit_count + "_tol/i").toDouble()*dtr;
    orbit.elem_tol[5] =  orbit_list.value(orbit_count + "_tol/node").toDouble()*dtr;
    orbit.elem_tol[6] =     orbit_list.value(orbit_count + "_tol/w").toDouble()*dtr;
    orbit.elem_tol[7] =     orbit_list.value(orbit_count + "_tol/u").toDouble()*dtr;
    orbit.elem_tol[8] =  orbit_list.value(orbit_count + "_tol/teta").toDouble()*dtr;

    orbit.calc_flag = 1;
    ElemToVect(orbit.elem_ref, orbit.vect_ref);
    for(int i = 0; i < 6; i++) orbit.vect_tmi[i] = rV[i];


    QSettings settings("settings/settings.ini", QSettings::IniFormat);

    QDateTime dtst = settings.value("start/dtst").toDateTime();
    t_dif = settings.value("start/t_dif").toDouble();

    jdst = ToJulianDate(dtst);

    double fi =  settings.value("start/fi").toDouble()*dtr;
    double lam = settings.value("start/lam").toDouble()*dtr;
    double psi = settings.value("start/psi").toDouble()*dtr;

    double tk = orbit.t_ref; double t0 = orbit.t_tmi+t_dif;
    double rVG[6];

    Mst2GrF(t_dif, fi, lam, psi, rV, rVG);

    double h = 1.0;
    int n = 6, kp = 1, np[13];
    double *p = new double[8*n+9];
    double *eps = new double[n];

    for (int i = 0; i < n; i++) {
        eps[i] = 1e-12;
    }

    rk8cc(n, t0, rVG, tk, func3, h, p, np, kp, eps);

    for(int i = 0; i < 6; i++) orbit.vect_calc[i] = rVG[i];
    VectToElem(rVG, orbit.elem_tmi);

    orbit.period_tmi = (pi_2*pow(orbit.elem_tmi[0], 3.0/2.0)/sqrt(mu3));

    for(int i = 4; i < 9; i++) orbit.elem_tmi[i] *=rtd;
    for(int i = 4; i < 9; i++) orbit.elem_ref[i] *=rtd;
    for(int i = 4; i < 9; i++) orbit.elem_tol[i] *=rtd;
    orbit.calc_dif();

    delete [] eps;
    delete [] p;

    QVector <Orbit> list;
    list.append(orbit);

    emit result_clear();
    for(int i = 0; i < list.count(); i++) emit send_orbit(list[i]);
    emit result_show();
    list.clear();
}

void MainWindow::on_button_calc_100_clicked() {

    QString orbit_count;
    Orbit orbit;
    double rV[6];

    QSettings orbit_list("settings/orbitlist.ini", QSettings::IniFormat);
    orbit_count = QString::number(ui->cbox_orbit_100->currentIndex() + 1);

    orbit.orbit_name = orbit_list.value(orbit_count + "/orbit_name").toString();
    orbit.scraft_name = orbit_list.value(orbit_count + "/scraft_name").toString();

    orbit.t_tmi = ui->ledit_tTMI_100->text().toDouble();
    rV[0] = ui->ledit_Rx_100->text().toDouble();
    rV[1] = ui->ledit_Ry_100->text().toDouble();
    rV[2] = ui->ledit_Rz_100->text().toDouble();
    rV[3] = ui->ledit_Vx_100->text().toDouble();
    rV[4] = ui->ledit_Vy_100->text().toDouble();
    rV[5] = ui->ledit_Vz_100->text().toDouble();

    orbit.period_ref = TimeToSeconds(ui->tedit_period_100->time());
    orbit.t_ref = ui->ledit_tREF_100->text().toDouble();
    orbit.elem_ref[0] =    ui->ledit_a_100->text().toDouble();
    orbit.elem_ref[1] =    ui->ledit_e_100->text().toDouble();
    orbit.elem_ref[2] =   ui->ledit_hp_100->text().toDouble();
    orbit.elem_ref[3] =   ui->ledit_ha_100->text().toDouble();
    orbit.elem_ref[4] =    ui->ledit_i_100->text().toDouble()*dtr;
    orbit.elem_ref[5] = ui->ledit_node_100->text().toDouble()*dtr;
    orbit.elem_ref[6] =    ui->ledit_w_100->text().toDouble()*dtr;
    orbit.elem_ref[7] =    ui->ledit_u_100->text().toDouble()*dtr;
    orbit.elem_ref[8] = ui->ledit_teta_100->text().toDouble()*dtr;

    orbit.period_tol = orbit_list.value(orbit_count + "_tol/period").toDouble();
    orbit.elem_tol[0] =     orbit_list.value(orbit_count + "_tol/a").toDouble();
    orbit.elem_tol[1] =     orbit_list.value(orbit_count + "_tol/e").toDouble();
    orbit.elem_tol[2] =    orbit_list.value(orbit_count + "_tol/hp").toDouble();
    orbit.elem_tol[3] =    orbit_list.value(orbit_count + "_tol/ha").toDouble();
    orbit.elem_tol[4] =     orbit_list.value(orbit_count + "_tol/i").toDouble()*dtr;
    orbit.elem_tol[5] =  orbit_list.value(orbit_count + "_tol/node").toDouble()*dtr;
    orbit.elem_tol[6] =     orbit_list.value(orbit_count + "_tol/w").toDouble()*dtr;
    orbit.elem_tol[7] =     orbit_list.value(orbit_count + "_tol/u").toDouble()*dtr;
    orbit.elem_tol[8] =  orbit_list.value(orbit_count + "_tol/teta").toDouble()*dtr;

    orbit.calc_flag = 0;
    ElemToVect(orbit.elem_ref, orbit.vect_ref);
    for(int i = 0; i < 6; i++) orbit.vect_tmi[i] = rV[i];


    QSettings settings("settings/settings.ini", QSettings::IniFormat);
    QDateTime dtst = settings.value("start/dtst").toDateTime();
    t_dif = settings.value("start/t_dif").toDouble();
    jdst = ToJulianDate(dtst);

    double tk = orbit.t_ref; double t0 = orbit.t_tmi+t_dif;
    double rVG[6];

    X6Asn2GrF(t0, rV, rVG);

    double h = 1.0;
    int n = 6, kp = 1, np[13];
    double *p = new double[8*n+9];
    double *eps = new double[n];

    for (int i = 0; i < n; i++) {
        eps[i] = 1e-12;
    }

    rk8cc(n, t0, rVG, tk, func3, h, p, np, kp, eps);

    for(int i = 0; i < 6; i++) orbit.vect_calc[i] = rVG[i];
    VectToElem(rVG, orbit.elem_tmi);

    orbit.period_tmi = (pi_2*pow(orbit.elem_tmi[0], 3.0/2.0)/sqrt(mu3));

    for(int i = 4; i < 9; i++) orbit.elem_tmi[i] *=rtd;
    for(int i = 4; i < 9; i++) orbit.elem_ref[i] *=rtd;
    for(int i = 4; i < 9; i++) orbit.elem_tol[i] *=rtd;
    orbit.calc_dif();

    delete [] eps;
    delete [] p;

    QVector <Orbit> list;
    list.append(orbit);

    emit result_clear();
    for(int i = 0; i < list.count(); i++) emit send_orbit(list[i]);
    emit result_show();
    list.clear();
}

void MainWindow::ledit_100_changed() {

    if (ui->ledit_tTMI_100->text() != "" &&
            ui->ledit_Rx_100->text() != "" &&
            ui->ledit_Ry_100->text() != "" &&
            ui->ledit_Rz_100->text() != "" &&
            ui->ledit_Vx_100->text() != "" &&
            ui->ledit_Vy_100->text() != "" &&
            ui->ledit_Vz_100->text() != "") {

        ui->button_calc_100->setEnabled(true);
    } else {

        ui->button_calc_100->setEnabled(false);
    }
}

void MainWindow::ledit_43_changed() {

    if (ui->ledit_tTMI_43->text() != "" &&
            ui->ledit_Rx_43->text() != "" &&
            ui->ledit_Ry_43->text() != "" &&
            ui->ledit_Rz_43->text() != "" &&
            ui->ledit_Vx_43->text() != "" &&
            ui->ledit_Vy_43->text() != "" &&
            ui->ledit_Vz_43->text() != "") {

        ui->button_calc_43->setEnabled(true);
    } else {

        ui->button_calc_43->setEnabled(false);
    }
}

void MainWindow::on_action_quit_triggered() {

    this->close();
}

void MainWindow::on_action_open_triggered() {

    if(ui->tab_widget->currentIndex() == 0) {
        ui->button_open_100->click();
    } else if(ui->tab_widget->currentIndex() == 1) {
        ui->button_open_43->click();
    }
}
