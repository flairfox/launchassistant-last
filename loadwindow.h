#ifndef LOADWINDOW_H
#define LOADWINDOW_H

#include <QWidget>

namespace Ui {
class LoadWindow;
}

class LoadWindow : public QWidget {

    Q_OBJECT

public:

    explicit LoadWindow(QWidget *parent = 0);
    ~LoadWindow();

public slots:

    void recieve_43_request (QString fname, double current_time);

    void recieve_100_request (QString fname, double current_time);

signals:

    void send_43_vect(double *vect);

    void send_100_vect(double *vect);

private slots:

    void on_button_ok_clicked();

    void on_button_cancel_clicked();

private:

    Ui::LoadWindow *ui;
};

#endif // LOADWINDOW_H
