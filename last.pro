#-------------------------------------------------
#
# Project created by QtCreator 2016-02-03T10:37:49
#
#-------------------------------------------------

QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = last
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    resultwindow.cpp \
    settingswindow.cpp \
    editwindow.cpp \
    loadwindow.cpp \
    constants.cpp \
    conversions.cpp \
    functions.cpp \
    jpleph.cpp \
    RK8CC.cpp \
    orbit.cpp \
    spaceportwindow.cpp \
    separationwindow.cpp

HEADERS  += mainwindow.h \
    resultwindow.h \
    settingswindow.h \
    editwindow.h \
    loadwindow.h \
    constants.h \
    conversions.h \
    functions.h \
    jpleph.h \
    jplint.h \
    RK8CC.h \
    orbit.h \
    spaceportwindow.h \
    separationwindow.h

FORMS    += mainwindow.ui \
    resultwindow.ui \
    settingswindow.ui \
    editwindow.ui \
    loadwindow.ui \
    spaceportwindow.ui \
    separationwindow.ui

RESOURCES += \
    lastres.qrc

RC_FILE = last_ico.rc
