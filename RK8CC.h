﻿//---------------------------------------------------------------------------

#ifndef RK8CCH
#define RK8CCH
//---------------------------------------------------------------------------

void rk8cc(int n, double &t0, double *x0, double t, int (*fun)(double *, double *, double *),
		   double h, double *p, int *np, int kp, double *e );

#endif
