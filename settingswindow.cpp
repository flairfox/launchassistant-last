#include "settingswindow.h"
#include "ui_settingswindow.h"

#include "mainwindow.h"
#include "editwindow.h"
#include "spaceportwindow.h"
#include "orbit.h"

#include <QCoreApplication>
#include <QTreeWidgetItem>
#include <QSettings>
#include <QString>
#include <QMap>

SettingsWindow::SettingsWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsWindow) {

    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowCloseButtonHint);

    edit_window = new EditWindow();
    spaceport_window = new SpaceportWindow();
    separation_window = new SeparationWindow();

    connect(this, SIGNAL(send_orbit(Orbit)), edit_window, SLOT(recieve_orbit(Orbit)));
    connect(this, SIGNAL(send_orbit(Orbit)), edit_window, SLOT(show()));

    connect(ui->list_widget, SIGNAL(doubleClicked(QModelIndex)), ui->button_edit, SLOT(click()));
    connect(this, SIGNAL(send_request()), edit_window, SLOT(recieve_request()));
    connect(this, SIGNAL(send_request()), edit_window, SLOT(show()));

    connect(edit_window, SIGNAL(send_edited(Orbit)), this, SLOT(recieve_edited(Orbit)));
    connect(edit_window, SIGNAL(send_new(Orbit)), this, SLOT(recieve_new(Orbit)));

    connect(ui->toolButton, SIGNAL(clicked()), spaceport_window, SLOT(show()));
    connect(ui->pushButton, SIGNAL(clicked()), separation_window, SLOT(show()));
}

SettingsWindow::~SettingsWindow() {

    delete ui;

}

QMap <QString, Orbit> orbit_map;

void SettingsWindow::refresh_settings() {

    orbit_map.clear();
    ui->list_widget->clear();

    load_list();
    load_settings();
    ui->list_widget->setCurrentRow(0);

    ui->button_apply->setEnabled(false);
}

void SettingsWindow::load_settings() {

    QSettings settings("settings/settings.ini", QSettings::IniFormat);

    ui->dtedit->setDateTime(settings.value("start/dtst").toDateTime());
    ui->ledit_psi->setText(settings.value("start/psi").toString());
    ui->ledit_td->setText(settings.value("start/t_dif").toString());

}

void SettingsWindow::load_orbit(int count, Orbit &orbit) {

    //Загрузка орбиты с параметрами из "orbitlist.ini"

    QSettings orbit_list("settings/orbitlist.ini", QSettings::IniFormat);
    QString orbit_count = QString::number(count + 1);

    orbit.orbit_name = orbit_list.value(orbit_count + "/orbit_name").toString();
    orbit.scraft_name = orbit_list.value(orbit_count + "/scraft_name").toString();

    orbit.t_ref = orbit_list.value(orbit_count + "/t").toDouble();
    orbit.period_ref  = orbit_list.value(orbit_count + "/period").toDouble();
    orbit.elem_ref[0] = orbit_list.value(orbit_count + "/a").toDouble();
    orbit.elem_ref[1] = orbit_list.value(orbit_count + "/e").toDouble();
    orbit.elem_ref[2] = orbit_list.value(orbit_count + "/hp").toDouble();
    orbit.elem_ref[3] = orbit_list.value(orbit_count + "/ha").toDouble();
    orbit.elem_ref[4] = orbit_list.value(orbit_count + "/i").toDouble();
    orbit.elem_ref[5] = orbit_list.value(orbit_count + "/node").toDouble();
    orbit.elem_ref[6] = orbit_list.value(orbit_count + "/w").toDouble();
    orbit.elem_ref[7] = orbit_list.value(orbit_count + "/u").toDouble();
    orbit.elem_ref[8] = orbit_list.value(orbit_count + "/teta").toDouble();

    orbit.period_tol  = orbit_list.value(orbit_count + "_tol/period").toDouble();
    orbit.elem_tol[0] = orbit_list.value(orbit_count + "_tol/a").toDouble();
    orbit.elem_tol[1] = orbit_list.value(orbit_count + "_tol/e").toDouble();
    orbit.elem_tol[2] = orbit_list.value(orbit_count + "_tol/hp").toDouble();
    orbit.elem_tol[3] = orbit_list.value(orbit_count + "_tol/ha").toDouble();
    orbit.elem_tol[4] = orbit_list.value(orbit_count + "_tol/i").toDouble();
    orbit.elem_tol[5] = orbit_list.value(orbit_count + "_tol/node").toDouble();
    orbit.elem_tol[6] = orbit_list.value(orbit_count + "_tol/w").toDouble();
    orbit.elem_tol[7] = orbit_list.value(orbit_count + "_tol/u").toDouble();
    orbit.elem_tol[8] = orbit_list.value(orbit_count + "_tol/teta").toDouble();
}

void SettingsWindow::load_list() {

    QSettings orbit_list("settings/orbitlist.ini", QSettings::IniFormat);
    n = orbit_list.value("0/n").toInt();

    Orbit orbit;

    for (int i = 0; i < n; i++) {
        load_orbit(i, orbit);
        orbit_map.insert(orbit.orbit_name, orbit);
        ui->list_widget->addItem(orbit.orbit_name);
    }
}

void SettingsWindow::on_button_up_clicked() {

    int current_index = ui->list_widget->currentRow();

    if (current_index == 0) return;

    QListWidgetItem *current_item = ui->list_widget->takeItem(current_index);
    ui->list_widget->insertItem(current_index - 1, current_item);
    ui->list_widget->setCurrentRow(current_index - 1);

    ui->button_apply->setEnabled(true);
}

void SettingsWindow::on_button_down_clicked() {

    int current_index = ui->list_widget->currentRow();

    if (current_index == ui->list_widget->count() - 1) return;

    QListWidgetItem *current_item = ui->list_widget->takeItem(current_index);
    ui->list_widget->insertItem(current_index + 1, current_item);
    ui->list_widget->setCurrentRow(current_index + 1);

    ui->button_apply->setEnabled(true);
}

void SettingsWindow::on_button_del_clicked() {

    if (ui->list_widget->count() == 0) return;
    orbit_map.remove(ui->list_widget->currentItem()->text());
    delete ui->list_widget->currentItem();
    n--;

    ui->button_apply->setEnabled(true);
}

void SettingsWindow::on_button_edit_clicked() {

    Orbit orbit;

    orbit = orbit_map.value(ui->list_widget->currentItem()->text());
    emit send_orbit(orbit);
}

void SettingsWindow::recieve_edited(Orbit orbit) {

    orbit_map.remove(ui->list_widget->currentItem()->text());
    orbit_map.insert(orbit.orbit_name, orbit);
    ui->list_widget->currentItem()->setText(orbit.orbit_name);

    ui->button_apply->setEnabled(true);
}

void SettingsWindow::on_button_add_clicked() {

    emit send_request();
}

void SettingsWindow::recieve_new(Orbit orbit) {

    orbit_map.insert(orbit.orbit_name, orbit);
    ui->list_widget->addItem(orbit.orbit_name);
    n++;

    ui->button_apply->setEnabled(true);
}

void SettingsWindow::write_list() {

    QSettings orbit_list("settings/orbitlist.ini", QSettings::IniFormat);
    orbit_list.clear();

    orbit_list.setValue("0/n", n);
    for (int i = 0; i < n; i++) {

        Orbit orbit = orbit_map.value(ui->list_widget->item(i)->text());

        orbit_list.setValue(QString::number(i+1) + "/orbit_name", orbit.orbit_name);
        orbit_list.setValue(QString::number(i+1) + "/scraft_name", orbit.scraft_name);

        orbit_list.setValue(QString::number(i+1) + "/period", QString::number(orbit.period_ref, 'f', 7));

        orbit_list.setValue(QString::number(i+1) + "/t",    QString::number(orbit.t_ref,       'f', 7));
        orbit_list.setValue(QString::number(i+1) + "/a",    QString::number(orbit.elem_ref[0], 'f', 7));
        orbit_list.setValue(QString::number(i+1) + "/e",    QString::number(orbit.elem_ref[1], 'f', 7));
        orbit_list.setValue(QString::number(i+1) + "/hp",   QString::number(orbit.elem_ref[2], 'f', 7));
        orbit_list.setValue(QString::number(i+1) + "/ha",   QString::number(orbit.elem_ref[3], 'f', 7));
        orbit_list.setValue(QString::number(i+1) + "/i",    QString::number(orbit.elem_ref[4], 'f', 7));
        orbit_list.setValue(QString::number(i+1) + "/node", QString::number(orbit.elem_ref[5], 'f', 7));
        orbit_list.setValue(QString::number(i+1) + "/w",    QString::number(orbit.elem_ref[6], 'f', 7));
        orbit_list.setValue(QString::number(i+1) + "/u",    QString::number(orbit.elem_ref[7], 'f', 7));
        orbit_list.setValue(QString::number(i+1) + "/teta", QString::number(orbit.elem_ref[8], 'f', 7));

        if (orbit.period_tol  != 0.0) orbit_list.setValue(QString::number(i+1) + "_tol/period", QString::number(orbit.period_tol, 'f', 7));

        if (orbit.elem_tol[0] != 0.0) orbit_list.setValue(QString::number(i+1) + "_tol/a",    QString::number(orbit.elem_tol[0], 'f', 7));
        if (orbit.elem_tol[1] != 0.0) orbit_list.setValue(QString::number(i+1) + "_tol/e",    QString::number(orbit.elem_tol[1], 'f', 7));
        if (orbit.elem_tol[2] != 0.0) orbit_list.setValue(QString::number(i+1) + "_tol/hp",   QString::number(orbit.elem_tol[2], 'f', 7));
        if (orbit.elem_tol[3] != 0.0) orbit_list.setValue(QString::number(i+1) + "_tol/ha",   QString::number(orbit.elem_tol[3], 'f', 7));
        if (orbit.elem_tol[4] != 0.0) orbit_list.setValue(QString::number(i+1) + "_tol/i",    QString::number(orbit.elem_tol[4], 'f', 7));
        if (orbit.elem_tol[5] != 0.0) orbit_list.setValue(QString::number(i+1) + "_tol/node", QString::number(orbit.elem_tol[5], 'f', 7));
        if (orbit.elem_tol[6] != 0.0) orbit_list.setValue(QString::number(i+1) + "_tol/w",    QString::number(orbit.elem_tol[6], 'f', 7));
        if (orbit.elem_tol[7] != 0.0) orbit_list.setValue(QString::number(i+1) + "_tol/u",    QString::number(orbit.elem_tol[7], 'f', 7));
        if (orbit.elem_tol[8] != 0.0) orbit_list.setValue(QString::number(i+1) + "_tol/teta", QString::number(orbit.elem_tol[8], 'f', 7));
    }
}

void SettingsWindow::write_settings() {

    QSettings settings("settings/settings.ini", QSettings::IniFormat);

    settings.setValue("start/dtst", ui->dtedit->dateTime());
    settings.setValue("start/psi", ui->ledit_psi->text());
    settings.setValue("start/t_dif", ui->ledit_td->text());
}

void SettingsWindow::on_button_apply_clicked() {

    write_list();
    write_settings();

    ui->button_apply->setEnabled(false);
}

void SettingsWindow::on_button_ok_clicked() {

    write_list();
    write_settings();
    this->close();

    emit reload_list();
}

void SettingsWindow::on_button_cancel_clicked() {

    this->close();
}
