#ifndef uFuncH
#define uFuncH

#include "jpleph.h"

int func30( double *t, double *x, double *f );
int func3 ( double *t, double *x, double *f );
void u_pl(double *x, double *xp, double const &gmp, double *u);
void HRM8( double *r, double *u );

#endif
