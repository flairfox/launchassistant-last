#include "spaceportwindow.h"
#include "ui_spaceportwindow.h"

#include <QSettings>
#include <QString>
#include <QMap>

SpaceportWindow::SpaceportWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SpaceportWindow) {

    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowCloseButtonHint);

    load_spaceports();
    ui->list_widget->setCurrentRow(0);
    prev_item = ui->list_widget->currentItem()->text();
    select_spaceport();

    ui->pushButton_2->setEnabled(false);

    connect(ui->lineEdit, SIGNAL(textChanged(QString)), this, SLOT(on_edits_changed()));
    connect(ui->lineEdit_2, SIGNAL(textChanged(QString)), this, SLOT(on_edits_changed()));
    connect(ui->lineEdit_3, SIGNAL(textChanged(QString)), this, SLOT(on_edits_changed()));
}

SpaceportWindow::~SpaceportWindow() {
    delete ui;
}

struct Spaceport {
    QString name;
    double fi, lam;
};

QMap <QString, Spaceport> spaceport_map;

void SpaceportWindow::load_spaceports() {

    ui->list_widget->clear();

    QSettings settings("settings/settings.ini", QSettings::IniFormat);
    n = settings.value("main/n").toDouble();

    for(int i = 0; i < n; i++) {
        Spaceport spaceport;
        spaceport.name = settings.value(QString::number(i) + "/name").toString();
        spaceport.fi = settings.value(QString::number(i) + "/fi").toDouble();
        spaceport.lam = settings.value(QString::number(i) + "/lam").toDouble();

        spaceport_map.insert(spaceport.name, spaceport);
        ui->list_widget->addItem(spaceport.name);
    }
}

void SpaceportWindow::select_spaceport() {

    Spaceport spaceport;

    spaceport = spaceport_map.value(ui->list_widget->currentItem()->text());
    ui->lineEdit->setText(spaceport.name);
    ui->lineEdit_2->setText(QString::number(spaceport.fi));
    ui->lineEdit_3->setText(QString::number(spaceport.lam));
}

void SpaceportWindow::on_list_widget_itemPressed() {

    Spaceport spaceport;

    spaceport = spaceport_map.value(prev_item);
    spaceport_map.remove(spaceport.name);

    spaceport.name = ui->lineEdit->text();
    spaceport.fi   = ui->lineEdit_2->text().toDouble();
    spaceport.lam  = ui->lineEdit_3->text().toDouble();

    spaceport_map.insert(spaceport.name, spaceport);

    for(int i = 0; i < ui->list_widget->count(); i++) {
        if(ui->list_widget->item(i)->text() == prev_item) {
            ui->list_widget->item(i)->setText(spaceport.name);
        }
    }

    select_spaceport();
    prev_item = ui->list_widget->currentItem()->text();
}

void SpaceportWindow::on_button_add_clicked() {

    Spaceport spaceport;

    for(int i = 0; i < ui->list_widget->count(); i++) {
        if(ui->lineEdit->text() == ui->list_widget->item(i)->text()) {
            return;
        }
    }

    spaceport.name = ui->lineEdit->text();
    spaceport.fi = ui->lineEdit_2->text().toDouble();
    spaceport.lam = ui->lineEdit_3->text().toDouble();

    spaceport_map.insert(spaceport.name, spaceport);
    ui->list_widget->addItem(spaceport.name);
    n++;

    ui->pushButton_2->setEnabled(true);
}

void SpaceportWindow::on_button_del_clicked() {

    if (ui->list_widget->count() == 0) return;
    spaceport_map.remove(ui->list_widget->currentItem()->text());
    delete ui->list_widget->currentItem();
    n--;

    ui->pushButton_2->setEnabled(true);
}

void SpaceportWindow::on_button_up_clicked() {

    int current_index = ui->list_widget->currentRow();

    if (current_index == 0) return;

    QListWidgetItem *current_item = ui->list_widget->takeItem(current_index);
    ui->list_widget->insertItem(current_index - 1, current_item);
    ui->list_widget->setCurrentRow(current_index - 1);

    ui->pushButton_2->setEnabled(true);
}

void SpaceportWindow::on_button_down_clicked() {

    int current_index = ui->list_widget->currentRow();

    if (current_index == ui->list_widget->count() - 1) return;

    QListWidgetItem *current_item = ui->list_widget->takeItem(current_index);
    ui->list_widget->insertItem(current_index + 1, current_item);
    ui->list_widget->setCurrentRow(current_index + 1);

    ui->pushButton_2->setEnabled(true);
}

void SpaceportWindow::on_edits_changed() {

    if(ui->lineEdit->text() != "" &&
       ui->lineEdit_2->text() != "" &&
       ui->lineEdit_3->text() != "") {

        ui->pushButton_2->setEnabled(true);
        ui->button_add->setEnabled(true);
    } else {

        ui->pushButton_2->setEnabled(false);
        ui->button_add->setEnabled(false);
    }
}
