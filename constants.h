﻿#ifndef uConstH
#define uConstH

#include "QTime"
#include "QString"

extern const double pi,			// Pi
					pi_2,      	// 2 Pi
					pi_pop,	    // Pi/2
					AE,        	// Астроном. единица (км)
					R_Earth,    // Экв. радиус Земли (км)
                    R3,
					R_Sun,      // радиус Солнца (км)
					R_Moon,     // радиус Луны (км)
					mu3,        // гравитационная постоянная Земли
					mum,        // гравитационная постоянная Луны
					mus,		// гравитационная постоянная Солнца
					g3,	     	// ускорение свободного падения на земле(экв), км/с/с
					rtd,		// коэффициент перевода радиан в градусы
					dtr,		// коэффициент перевода градусов в радианы
					w_earth,	// скорость вращения земли
					w_earth_1,
                    w_moon,
                    e_szh,
                    PZ90_a,
                    PZ90_alfa,
					dTef,
                    sut,
					dTef_sut;

extern const double	coef_a1[106],
					coef_a2[106],
					coef_a3[106],
					coef_a4[106],
					coef_a5[106],
					coef_A[106],
					coef_B[106],
					coef_C[106],
					coef_D[106];

extern double t_dif, jdst;
#endif
