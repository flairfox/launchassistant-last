#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QWidget>
#include <QTreeWidgetItem>

#include "orbit.h"
#include "editwindow.h"
#include "separationwindow.h"
#include "spaceportwindow.h"

namespace Ui {
class SettingsWindow;
}

class SettingsWindow : public QWidget
{
    Q_OBJECT

public:

    explicit SettingsWindow(QWidget *parent = 0);
    ~SettingsWindow();

public slots:

    void refresh_settings();

    void recieve_edited(Orbit orbit);

    void recieve_new(Orbit orbit);

private slots:

    void on_button_up_clicked();

    void on_button_down_clicked();

    void on_button_del_clicked();

    void on_button_edit_clicked();

    void on_button_add_clicked();

    void on_button_apply_clicked();

    void on_button_ok_clicked();

    void on_button_cancel_clicked();

signals:

    void send_orbit(Orbit orbit);

    void send_request();

    void reload_list();

private:

    Ui::SettingsWindow *ui;

    EditWindow *edit_window;
    SpaceportWindow *spaceport_window;
    SeparationWindow *separation_window;

    int n;

    void load_orbit(int count, Orbit &orbit);

    void load_settings();

    void load_list();

    void write_list();

    void write_settings();
};

#endif // SETTINGSWINDOW_H
