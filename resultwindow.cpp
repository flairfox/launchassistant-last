#include "resultwindow.h"
#include "ui_resultwindow.h"
#include "conversions.h"
#include "constants.h"

#include "orbit.h"

#include <QFileDialog>
#include <QTextDocument>
#include <QPrinterInfo>
#include <QTableWidget>
#include <QStringList>
#include <QTextStream>
#include <QSettings>
#include <QPrinter>
#include <QVector>
#include <QColor>
#include <QFile>

ResultWindow::ResultWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ResultWindow) {
    ui->setupUi(this);

    this->setWindowFlags(Qt::WindowCloseButtonHint);

    QString defaultPrinter;
    QStringList printerList;
    QPrinterInfo printerInfo;

    printerList = printerInfo.availablePrinterNames();
    int printerCount = printerList.size();

    for (int i = 0; i < printerCount; i++) {
        ui->printerList->addItem(printerList[i]);
    }

    defaultPrinter = printerInfo.defaultPrinterName();
    ui->printerList->setCurrentText(defaultPrinter);
    ui->printerList->view()->setMinimumWidth(ui->printerList->view()->sizeHintForColumn(0));
}

ResultWindow::~ResultWindow() {
    delete ui;
}

QVector <Orbit> orbit_list;
QVector <QString> html_list;
QString file_name;

void ResultWindow::result_clear() {

    orbit_list.clear();
    html_list.clear();
}

void ResultWindow::recieve_orbit(Orbit orbit) {

    orbit_list.append(orbit);
}

void ResultWindow::result_show() {

    draw_tabs();
}

void ResultWindow::draw_tabs() {

    for(int i = 0; i < ui->tabWidget->count()+1; i++) {

        ui->tabWidget->setCurrentIndex(i);
        delete ui->tabWidget->currentWidget();
    }

    foreach(Orbit orbit, orbit_list) {

        html_list.append(fill_html(orbit));
        addTabs(orbit);
    }

    QString calc_type;
    if(orbit_list[0].calc_flag == 1) {
        calc_type = "Обработка данных СУ РБФ.";
    } else {
        calc_type = "Обработка данных АСН.";
    }

    ui->label->setText("Орбита: " + orbit_list[0].orbit_name + ". " + calc_type);
}

void fill_str (Orbit orbit, QString str[10][5]) {

    // Заполняем номинальные параметры орбиты
    str[0][0] = SecondsToTime(orbit.period_ref).toString("hh:mm:ss.zzz");
    str[1][0] = QString::number(orbit.elem_ref[0], 'f', 1);
    str[2][0] = QString::number(orbit.elem_ref[1], 'f', 5);
    str[3][0] = QString::number(orbit.elem_ref[2], 'f', 1);
    str[4][0] = QString::number(orbit.elem_ref[3], 'f', 1);
    str[5][0] = QString::number(orbit.elem_ref[4], 'f', 2);
    str[6][0] = QString::number(orbit.elem_ref[5], 'f', 2);
    str[7][0] = QString::number(orbit.elem_ref[6], 'f', 2);
    str[8][0] = QString::number(orbit.elem_ref[7], 'f', 2);
    str[9][0] = QString::number(orbit.elem_ref[8], 'f', 2);

    // Заполняем фактические параметры орбиты
    str[0][1] = SecondsToTime(orbit.period_tmi).toString("hh:mm:ss.zzz");
    str[1][1] = QString::number(orbit.elem_tmi[0], 'f', 1);
    str[2][1] = QString::number(orbit.elem_tmi[1], 'f', 5);
    str[3][1] = QString::number(orbit.elem_tmi[2], 'f', 1);
    str[4][1] = QString::number(orbit.elem_tmi[3], 'f', 1);
    str[5][1] = QString::number(orbit.elem_tmi[4], 'f', 2);
    str[6][1] = QString::number(orbit.elem_tmi[5], 'f', 2);
    str[7][1] = QString::number(orbit.elem_tmi[6], 'f', 2);
    str[8][1] = QString::number(orbit.elem_tmi[7], 'f', 2);
    str[9][1] = QString::number(orbit.elem_tmi[8], 'f', 2);

    // Заполняем фактические отклонения
    str[0][2] = QString::number(orbit.period_dif, 'f', 1)+" c";
    str[1][2] = QString::number(orbit.elem_dif[0], 'f', 1);
    str[2][2] = QString::number(orbit.elem_dif[1], 'f', 5);
    str[3][2] = QString::number(orbit.elem_dif[2], 'f', 1);
    str[4][2] = QString::number(orbit.elem_dif[3], 'f', 1);
    str[5][2] = QString::number(orbit.elem_dif[4], 'f', 2);
    str[6][2] = QString::number(orbit.elem_dif[5], 'f', 2);
    str[7][2] = QString::number(orbit.elem_dif[6], 'f', 2);
    str[8][2] = QString::number(orbit.elem_dif[7], 'f', 2);
    str[9][2] = QString::number(orbit.elem_dif[8], 'f', 2);

    // Заполняем допустимые отклонения и заключение о нахождении в допуске
    int n;

    // Период
    n = 0;
    str[n][4] = orbit.tol_flag.at(n);
    if(orbit.tol_flag.at(n) == "") {
        str[n][3] = "-";
    } else {
        str[n][3] = QString::number(orbit.period_tol, 'f', 1) + " c";
        str[n][4] = orbit.tol_flag.at(n);
    }

    // Большая полуось
    n = 1;
    str[n][4] = orbit.tol_flag.at(n);
    if(orbit.tol_flag.at(n) == "") {
        str[n][3] = "-";
    } else {
        str[n][3] = QString::number(orbit.elem_tol[n-1], 'f', 1);
        str[n][4] = orbit.tol_flag.at(n);
    }

    // Эксцентриситет
    n = 2;
    str[n][4] = orbit.tol_flag.at(n);
    if(orbit.tol_flag.at(n) == "") {
        str[n][3] = "-";
    } else {
        str[n][3] = QString::number(orbit.elem_tol[n-1], 'f', 5);
        str[n][4] = orbit.tol_flag.at(n);
    }

    // Высота перигея
    n = 3;
    str[n][4] = orbit.tol_flag.at(n);
    if(orbit.tol_flag.at(n) == "") {
        str[n][3] = "-";
    } else {
        str[n][3] = QString::number(orbit.elem_tol[n-1], 'f', 1);
        str[n][4] = orbit.tol_flag.at(n);
    }

    // Высота апогея
    n = 4;
    str[n][4] = orbit.tol_flag.at(n);
    if(orbit.tol_flag.at(n) == "") {
        str[n][3] = "-";
    } else {
        str[n][3] = QString::number(orbit.elem_tol[n-1], 'f', 1);
        str[n][4] = orbit.tol_flag.at(n);
    }

    // Наклонение
    n = 5;
    str[n][4] = orbit.tol_flag.at(n);
    if(orbit.tol_flag.at(n) == "") {
        str[n][3] = "-";
    } else {
        str[n][3] = QString::number(orbit.elem_tol[n-1], 'f', 2);
        str[n][4] = orbit.tol_flag.at(n);
    }

    // Долгота восходящего узла
    n = 6;
    str[n][4] = orbit.tol_flag.at(n);
    if(orbit.tol_flag.at(n) == "") {
        str[n][3] = "-";
    } else {
        str[n][3] = QString::number(orbit.elem_tol[n-1], 'f', 2);
        str[n][4] = orbit.tol_flag.at(n);
    }

    // Агрумент перигея
    n = 7;
    str[n][4] = orbit.tol_flag.at(n);
    if(orbit.tol_flag.at(n) == "") {
        str[n][3] = "-";
    } else {
        str[n][3] = QString::number(orbit.elem_tol[n-1], 'f', 2);
        str[n][4] = orbit.tol_flag.at(n);
    }

    // Агрумент широты
    n = 8;
    str[n][4] = orbit.tol_flag.at(n);
    if(orbit.tol_flag.at(n) == "") {
        str[n][3] = "-";
    } else {
        str[n][3] = QString::number(orbit.elem_tol[n-1], 'f', 2);
        str[n][4] = orbit.tol_flag.at(n);
    }

    // Истинная аномалия
    n = 9;
    str[n][4] = orbit.tol_flag.at(n);
    if(orbit.tol_flag.at(n) == "") {
        str[n][3] = "-";
    } else {
        str[n][3] = QString::number(orbit.elem_tol[n-1], 'f', 2);
        str[n][4] = orbit.tol_flag.at(n);
    }
}

QString ResultWindow::fill_html (Orbit orbit) {

    QString str[10][5];
    QTime currentTime;
    QDateTime t_KP, t_OTP;

    fill_str(orbit, str);

    QString html_str;
    QStringList header;

    currentTime = QTime::currentTime();
    QSettings settings("settings/settings.ini", QSettings::IniFormat);
    QDateTime dtst = settings.value("start/dtst").toDateTime();
    QDateTime d_DMV = dtst.addSecs(3.0*3600.0);
    t_KP = dtst.addSecs(3.0*3600.0);
    t_OTP = t_KP.addMSecs(t_dif*1000.0 - 0.001);

    QString cs_type, calc_type;
    if(orbit.calc_flag == 0) {
        cs_type = "ГрСК";
        calc_type = "Обработка данных АСН.";
    } else if(orbit.calc_flag == 1){
        cs_type = "НССК";
        calc_type = "Обработка данных СУ РБФ.";
    }

    header.append("Оскулирующий период");
    header.append("Большая полуось, км");
    header.append("Эксцентриситет");
    header.append("Высота перигея, км");
    header.append("Высота апогея, км");
    header.append("Наклонение, град");
    header.append("Долгота восходящего узла, град");
    header.append("Аргумент перигея, град");
    header.append("Аргумент широты, град");
    header.append("Истинная аномалия, град");

    html_str.append("<html> \n");
    html_str.append("   <body> \n");
    html_str.append("       <center> \n");
    html_str.append("       <p align = center style = 'font-size: 16px;'> <b> Выведение космических аппаратов «Сентинел-1Б», «Микроскоп» и «Fly Your Satellite!» </b> \n");
    html_str.append("       <p align = center style = 'font-size: 14px;'> Дата старта: " + d_DMV.date().toString("dd.MM.yyyy") + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Момент КП (ДМВ): " + t_KP.time().toString("hh:mm:ss.zzz") + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Момент ОТП (ДМВ): " + " " + t_OTP.time().toString("hh:mm:ss.zzz") + " </p>");
    html_str.append("       <hr> \n");
    html_str.append("           <p align = center style = 'font-size: 16px;'> <b> Орбита: " + orbit.orbit_name + ". " + calc_type + "</b> \n");
    html_str.append("           <p align = center style = 'font-size: 14px;'> Время формирования документа: " + currentTime.toString("hh:mm:ss") + " </p> \n");
    html_str.append("           <p> </p> \n");

    html_str.append("           <table width = 85% align = center border = 1 cellpadding = 2 cellspacing = 0 style = 'font-size: 14px; border-color: black; border-style: solid'> \n");
    html_str.append("               <tr> \n");
    html_str.append("                   <td width = 30% bgcolor = #EFEFEF align = center> </td> \n");
    html_str.append("                   <td width = 14% bgcolor = #EFEFEF align = center> Ном. значения </td> \n");
    html_str.append("                   <td width = 14% bgcolor = #EFEFEF align = center> Факт. значения </td> \n");
    html_str.append("                   <td width = 14% bgcolor = #EFEFEF align = center> Отклонение </td> \n");
    html_str.append("                   <td width = 14% bgcolor = #EFEFEF align = center> Допуск </td> \n");
    html_str.append("                   <td width = 14% bgcolor = #EFEFEF align = center> </td> \n");
    html_str.append("               </tr> \n");

    for (int i = 0; i < 10; i++) {
        html_str.append("               <tr> \n");
        html_str.append("                   <td align = left bgcolor = #EFEFEF> " + header.at(i) + " </td> \n");

        for (int j = 0; j < 5; j++) {
            if (str[i][j] == "В допуске") {
                html_str.append("                   <td bgcolor = #CEF4D8 align = center> В допуске </td> \n");
            } else {
                if (str[i][j] == "Вне допуска!") {
                    html_str.append("                   <td bgcolor = #EEBDBB align = center> Вне допуска! </td> \n");
                } else {
                    html_str.append("                   <td align = center> " + str[i][j] + " </td> \n");
                }
            }
        }
        html_str.append("               </tr> \n");
    }
    html_str.append("           </table> \n");

    html_str.append("           </table> \n");

    html_str.append("           <p> <br> </p> \n");
    html_str.append("           <table width = 85% align = center border = 0 cellpadding = 4 cellspacing = 0 style = 'font-size: 14px;'> \n");
    html_str.append("               <tr> \n");
    html_str.append("                   <td width = 33%> Вектор, полученный из ТМИ (" + cs_type + "): </td> \n");
    html_str.append("                   <td width = 33%> Рассчитанный вектор (ГрКП): </td> \n");
    html_str.append("                   <td width = 33%> Номинальный вектор (ГрКП): </td> \n");
    html_str.append("               </tr> \n");

    html_str.append("               <tr> \n");
    html_str.append("                   <td> t = " + QString::number(orbit.t_tmi, 'f', 3) + " с </td> \n");
    html_str.append("                   <td> t = " + QString::number(orbit.t_ref, 'f', 3) + " с </td> \n");
    html_str.append("                   <td> t = " + QString::number(orbit.t_ref, 'f', 3) + " с </td> \n");
    html_str.append("               </tr> \n");

    html_str.append("               <tr> \n");
    html_str.append("                   <td> X = " + QString::number(orbit.vect_tmi[0], 'f', 3) + " км </td> \n");
    html_str.append("                   <td> X = " + QString::number(orbit.vect_calc[0], 'f', 3) + " км </td> \n");
    html_str.append("                   <td> X = " + QString::number(orbit.vect_ref[0], 'f', 3) + " км </td> \n");
    html_str.append("               </tr> \n");

    html_str.append("               <tr> \n");
    html_str.append("                   <td> Y = " + QString::number(orbit.vect_tmi[1], 'f', 3) + " км </td> \n");
    html_str.append("                   <td> Y = " + QString::number(orbit.vect_calc[1], 'f', 3) + " км </td> \n");
    html_str.append("                   <td> Y = " + QString::number(orbit.vect_ref[1], 'f', 3) + " км </td> \n");
    html_str.append("               </tr> \n");

    html_str.append("               <tr> \n");
    html_str.append("                   <td> Z = " + QString::number(orbit.vect_tmi[2], 'f', 3) + " км </td> \n");
    html_str.append("                   <td> Z = " + QString::number(orbit.vect_calc[2], 'f', 3) + " км </td> \n");
    html_str.append("                   <td> Z = " + QString::number(orbit.vect_ref[2], 'f', 3) + " км </td> \n");
    html_str.append("               </tr> \n");

    html_str.append("               <tr> \n");
    html_str.append("                   <td> V<sub>X</sub> = " + QString::number(orbit.vect_tmi[3]*1000.0, 'f', 3) + " м/с </td> \n");
    html_str.append("                   <td> V<sub>X</sub> = " + QString::number(orbit.vect_calc[3]*1000.0, 'f', 3) + " м/с </td> \n");
    html_str.append("                   <td> V<sub>X</sub> = " + QString::number(orbit.vect_ref[3]*1000.0, 'f', 3) + " м/с </td> \n");
    html_str.append("               </tr> \n");

    html_str.append("               <tr> \n");
    html_str.append("                   <td> V<sub>Y</sub> = " + QString::number(orbit.vect_tmi[4]*1000.0, 'f', 3) + " м/с </td> \n");
    html_str.append("                   <td> V<sub>Y</sub> = " + QString::number(orbit.vect_calc[4]*1000.0, 'f', 3) + " м/с </td> \n");
    html_str.append("                   <td> V<sub>Y</sub> = " + QString::number(orbit.vect_ref[4]*1000.0, 'f', 3) + " м/с </td> \n");
    html_str.append("               </tr> \n");

    html_str.append("               <tr> \n");
    html_str.append("                   <td> V<sub>Z</sub> = " + QString::number(orbit.vect_tmi[5]*1000.0, 'f', 3) + " м/с </td> \n");
    html_str.append("                   <td> V<sub>Z</sub> = " + QString::number(orbit.vect_calc[5]*1000.0, 'f', 3) + " м/с </td> \n");
    html_str.append("                   <td> V<sub>Z</sub> = " + QString::number(orbit.vect_ref[5]*1000.0, 'f', 3) + " м/с </td> \n");
    html_str.append("               </tr> \n");
    html_str.append("           </table> \n");

    html_str.append("           <p> <br> </p> \n");
    html_str.append("           <hr> \n");

    html_str.append("           <table align = right border = 0 cellpadding = 0 cellspacing = 0 style = 'font-size: 14px;'> \n");
    html_str.append("               <tr> \n");
    html_str.append("                   <td> <img src = ':/pics/pics/logo_laspace_bw.png' height = 25> </img> </td> \n");
    html_str.append("               </tr> \n");
    html_str.append("           </table> \n");

    html_str.append("       </center> \n");
    html_str.append("   </body> \n");
    html_str.append("</html>");

    file_name = orbit.orbit_name + ". " + calc_type + " (" + QString::number(orbit.t_tmi, 'f', 3) + ").pdf";

    return(html_str);
}

void ResultWindow::addTabs(Orbit orbit) {

    QTableWidget *newTable = new QTableWidget();
    ui->tabWidget->addTab(newTable, orbit.scraft_name);

    newTable->setColumnCount(5);
    newTable->setRowCount(10);
    newTable->setSelectionMode(QAbstractItemView::NoSelection);
    newTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    newTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    newTable->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    newTable->setFrameStyle(0);

    newTable->setHorizontalHeaderItem(0, new QTableWidgetItem("Ном. значение"));
    newTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Факт. значение"));
    newTable->setHorizontalHeaderItem(2, new QTableWidgetItem("Отклонение"));
    newTable->setHorizontalHeaderItem(3, new QTableWidgetItem("Допуск"));
    newTable->setHorizontalHeaderItem(4, new QTableWidgetItem(""));

    newTable->setVerticalHeaderItem(0, new QTableWidgetItem("Оскулирующий период"));
    newTable->setVerticalHeaderItem(1, new QTableWidgetItem("Большая полуось, км"));
    newTable->setVerticalHeaderItem(2, new QTableWidgetItem("Эксцентриситет"));
    newTable->setVerticalHeaderItem(3, new QTableWidgetItem("Высота перигея, км"));
    newTable->setVerticalHeaderItem(4, new QTableWidgetItem("Высота апогея, км"));
    newTable->setVerticalHeaderItem(5, new QTableWidgetItem("Наклонение, град"));
    newTable->setVerticalHeaderItem(6, new QTableWidgetItem("Долгота восходящего узла, град"));
    newTable->setVerticalHeaderItem(7, new QTableWidgetItem("Аргумент перигея, град"));
    newTable->setVerticalHeaderItem(8, new QTableWidgetItem("Аргумент широты, град"));
    newTable->setVerticalHeaderItem(9, new QTableWidgetItem("Истинная аномалия, град"));

    //Теперь заполняем таблицу

    QString str[10][5];
    fill_str(orbit, str);

    for (int i = 0; i < 10; i++){
        for (int j = 0; j < 5; j++){
            newTable->setItem(i, j, new QTableWidgetItem(str[i][j]));
            if(str[i][j] == "В допуске") {
                newTable->item(i, j)->setBackground(QColor(206, 244, 217));
                newTable->item(i, j)->setTextColor(QColor(44, 131, 79));
            }

            if(str[i][j] == "Вне допуска!") {
                newTable->item(i, j)->setBackground(QColor(238, 189, 187));
                newTable->item(i, j)->setTextColor(QColor(171, 24, 3));
            }

            newTable->item(i, j)->setTextAlignment(Qt::AlignCenter);
        }
    }
}

void ResultWindow::on_print_clicked() {

    //Собираем параметры для печати
    QPrinter printer;
    printer.setPrinterName(ui->printerList->currentText());
    printer.setOrientation(QPrinter::Landscape);
    printer.setPaperSize(QPrinter::A4);
    printer.setPageMargins(10.0, 12.0, 10.0, 8.0, QPrinter::Millimeter);
    printer.setCopyCount(ui->listNum->value());

    QSizeF paperSize;
    paperSize.setWidth(printer.width());
    paperSize.setHeight(printer.height());

    //Создаем HTML разметку
    QTextDocument print_doc(this);
    print_doc.setHtml(html_list[ui->tabWidget->currentIndex()]);

    //Вывод разметки в отдельный .html файл для контроля (закомментировано)
/*
    QString filename="table_layout.html";
    QFile file(filename);

    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        stream << html_list[ui->tabWidget->currentIndex()];
    }
*/

    print_doc.setPageSize(paperSize);
    print_doc.print(&printer);

    if (ui->checkSave->isChecked()) {
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName("results/" + file_name);
        print_doc.print(&printer);
    }
}

void ResultWindow::on_toolButton_clicked() {

    //Собираем параметры для печати
    QPrinter printer;

    printer.setOrientation(QPrinter::Landscape);
    printer.setPaperSize(QPrinter::A4);
    printer.setPageMargins(10.0, 12.0, 10.0, 8.0, QPrinter::Millimeter);
    printer.setCopyCount(ui->listNum->value());

    QSizeF paperSize;
    paperSize.setWidth(printer.width());
    paperSize.setHeight(printer.height());

    //Создаем HTML разметку
    QTextDocument print_doc(this);
    print_doc.setHtml(html_list[ui->tabWidget->currentIndex()]);

    print_doc.setPageSize(paperSize);
    printer.setOutputFormat(QPrinter::PdfFormat);
    QString fname = QFileDialog::getSaveFileName(this, "Сохранить в .pdf", "results/" + file_name, "*.pdf");
    if (fname == "") return;
    printer.setOutputFileName(fname);
    print_doc.print(&printer);
}

void ResultWindow::on_pushButton_clicked() {

    this->close();
}

void ResultWindow::on_rbutton_GrKP_clicked() {

    int current_tab = ui->tabWidget->currentIndex();
    html_list.clear();
    orbit_list[1].elem_ref[0] = 1.0;
    draw_tabs();
    ui->tabWidget->setCurrentIndex(current_tab);
}

void ResultWindow::on_rbutton_J2000_clicked() {

    int current_tab = ui->tabWidget->currentIndex();
    html_list.clear();
    //orbit_list[1].toJ2000();
    //Проверка работы системы контроля версий
    draw_tabs();
    ui->tabWidget->setCurrentIndex(current_tab);
}
