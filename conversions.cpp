﻿#include "conversions.h"
#include "constants.h"

#include <math.h>
#include <stdio.h>
#include <float.h>

#include <QDateTime>
#include <QTime>

using namespace std;

const double dUTC = 68.184/86400;
double UTCtoTDB(double JD){ return JD+dUTC; }
double TDBtoUTC(double JD){ return JD-dUTC; }

double TimeToSeconds(QTime t) {

    double seconds;

    seconds = t.hour()*3600.0+t.minute()*60.0+t.second()+t.msec()/1000.0;

    return seconds;
}

QTime SecondsToTime (double t) {

    //Перевод секунд в QTime

    double dseconds, seconds, minutes, hours;
    QTime time;

    hours = floor(t/3600.0);
    t = t-hours*3600.0;

    minutes = floor(t/60.0);
    t = t-minutes*60.0;

    seconds = floor(t/1.0);
    t = t - seconds;

    dseconds = round(t*1000.0);

    time.setHMS(hours, minutes, seconds, dseconds);

    return time;
}

double ToJulianDate(QDateTime t) {
    //Перевод в Юлианскую дату

    double Sec;
    int Year, Month, Day, Hour, Min;

    Year = t.date().year();
    Month = t.date().month();
    Day = t.date().day();
    Hour = t.time().hour();
    Min = t.time().minute();
    Sec = t.time().second();

    long MjdMidnight;
    int b;
    if (Month <= 2){ Month += 12; --Year; }
    if ((10000L * Year + 100L * Month + Day) <= 15821004L)
        b = -2 + ((Year + 4716) / 4) - 1179;
    else
        b = (Year / 400) - (Year / 100) + (Year / 4);
    MjdMidnight = 365L * Year - 679004 + b + int(30.6001*(Month + 1)) + Day;
    return MjdMidnight + ((Hour + Min / 60.0 + Sec / 3600.0) / 24.0) + 2400000.5;
}

double SetJulianDate(int Year, int Month, int Day, int Hour, int Min, double Sec) {
    //Перевод в Юлианскую дату

    long MjdMidnight;
    int b;
    if (Month <= 2){ Month += 12; --Year; }
    if ((10000L * Year + 100L * Month + Day) <= 15821004L)
        b = -2 + ((Year + 4716) / 4) - 1179;
    else
        b = (Year / 400) - (Year / 100) + (Year / 4);
    MjdMidnight = 365L * Year - 679004 + b + int(30.6001*(Month + 1)) + Day;
    return MjdMidnight + ((Hour + Min / 60.0 + Sec / 3600.0) / 24.0) + 2400000.5;
}

double ModulVec (double x[3]){
    // Расчет модуля вектора
    return sqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2]);
}

void ElemToVect(const double Ar1[9], double Ar2[6]){
    //Перевод элементов орбиты в вектор состояния

    //Ar1[0] = a            Ar2[0] = Rx
    //Ar1[1] = e            Ar2[1] = Ry
    //Ar1[2] = hp           Ar2[2] = Rz
    //Ar1[3] = ha           Ar2[3] = Vx
    //Ar1[4] = i            Ar2[4] = Vy
    //Ar1[5] = node         Ar2[5] = Vz
    //Ar1[6] = w
    //Ar1[7] = u
    //Ar1[8] = teta

    double a1, b1, g1, a2, b2, g2, tet, r, C, p;
    const double mu = 398600.4415;
    p = Ar1[0]*(1 - Ar1[1]*Ar1[1]);
    a1 = cos(Ar1[7])*cos(Ar1[5]) - sin(Ar1[7])*sin(Ar1[5])*cos(Ar1[4]);      //
    b1 = cos(Ar1[7])*sin(Ar1[5]) + sin(Ar1[7])*cos(Ar1[5])*cos(Ar1[4]);      // направляющие косинусы вектора r
    g1 = sin(Ar1[7])*sin(Ar1[4]);                                            //

    a2 = - (sin(Ar1[7])*cos(Ar1[5]) + cos(Ar1[7])*sin(Ar1[5])*cos(Ar1[4]));  //
    b2 = cos(Ar1[7])*cos(Ar1[5])*cos(Ar1[4]) - sin(Ar1[7])*sin(Ar1[5]);      // направляющие косинусы нормали к вект r
    g2 = cos(Ar1[7])*sin(Ar1[4]);                                            //

    tet = Ar1[7] - Ar1[6];
    C = sqrt(p*mu);
    r = p/(1 + Ar1[1]*cos(tet));

    Ar2[0] = r*a1;                                                   // X
    Ar2[1] = r*b1;                                                   // Y
    Ar2[2] = r*g1;                                                   // Z
    Ar2[3] = C*(a1*Ar1[1]*sin(tet) + a2*(1 + Ar1[1]*cos(tet)))/p;    // Vx
    Ar2[4] = C*(b1*Ar1[1]*sin(tet) + b2*(1 + Ar1[1]*cos(tet)))/p;    // Vy
    Ar2[5] = C*(g1*Ar1[1]*sin(tet) + g2*(1 + Ar1[1]*cos(tet)))/p;    // Vz
}

void VectToElem(const double Ar1[6], double Ar2[9]) {
    //Перевод вектора состояния в эелементы орбиты

    //Ar2[0] = a            Ar1[0] = Rx
    //Ar2[1] = e            Ar1[1] = Ry
    //Ar2[2] = hp           Ar1[2] = Rz
    //Ar2[3] = ha           Ar1[3] = Vx
    //Ar2[4] = i            Ar1[4] = Vy
    //Ar2[5] = node         Ar1[5] = Vz
    //Ar2[6] = w
    //Ar2[7] = u
    //Ar2[8] = teta

    double mu, r, C, C1, C2, C3, f, f1, f2, f3, p, Rp, Ra;
    mu = mu3;

    r = sqrt(Ar1[0]*Ar1[0] + Ar1[1]*Ar1[1] + Ar1[2]*Ar1[2]); // модуль в-ра r

    //Находим компоненты и модули вектора кинетического момента C и в-ра Лапласа f
    C1 = Ar1[1]*Ar1[5] - Ar1[2]*Ar1[4];
    C2 = Ar1[2]*Ar1[3] - Ar1[0]*Ar1[5];
    C3 = Ar1[0]*Ar1[4] - Ar1[1]*Ar1[3];
    C = sqrt(C1*C1 + C2*C2 + C3*C3);

    f1 = -mu*Ar1[0]/r + C3*Ar1[4] - C2*Ar1[5];
    f2 = -mu*Ar1[1]/r + C1*Ar1[5] - C3*Ar1[3];
    f3 = -mu*Ar1[2]/r + C2*Ar1[3] - C1*Ar1[4];
    f = sqrt(f1*f1 + f2*f2 + f3*f3);
    Ar2[1] = f/mu;                    // эксцентриситет е
    p = (C*C)/mu;                     // фокальный параметр р
    Ar2[0] = p/(1 - Ar2[1]*Ar2[1]);   // большая полуось орбиты a
    Ar2[4] = acos(C3/C);              // наклонение i [0;pi]

    Rp = Ar2[0]*(1.0 - Ar2[1]);
    Ra = Ar2[0]*(1.0 + Ar2[1]);
    Ar2[2] = Rp - R3;                 //Hp
    Ar2[3] = Ra - R3;                 //Ha


    //Находим node, w, и u в диапазоне [0;2pi] сравнивая синусы и косинусы
    double AC, AS;

    //Ar2[5] = node
    AS =   C1/(C*sin(Ar2[4]));
    AC = - C2/(C*sin(Ar2[4]));
    Ar2[5] = atan2(AS,AC);

    if(Ar2[5]<0) Ar2[5] += 360.0*dtr;

    //Ar2[6] = w
    AS = (-f1*sin(Ar2[5]) + f2*cos(Ar2[5]))/(f*cos(Ar2[4]));
    AC = ( f1*cos(Ar2[5]) + f2*sin(Ar2[5]))/f;
    Ar2[6] = atan2(AS,AC);

    if(Ar2[6]<0) Ar2[6] += 360.0*dtr;

    //Ar2[7] = u
    AS = (-Ar1[0]*sin(Ar2[5]) + Ar1[1]*cos(Ar2[5]))/(r*cos(Ar2[4]));
    AC = ( Ar1[0]*cos(Ar2[5]) + Ar1[1]*sin(Ar2[5]))/r;
    Ar2[7] = atan2(AS,AC);

    if(Ar2[7]<0) Ar2[7] += 360.0*dtr;

    //Ar2[8] = teta

    Ar2[8] = Ar2[7]-Ar2[6];

    if(Ar2[8]<0) Ar2[8] += 360.0*dtr;
}

void MatInv(double A[3][3], int N) {
    //Обращение матрицы 3x3

    double temp;
    double E[3][3]={0};

    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
        {
            E[i][j] = 0.0;

            if (i == j)
                E[i][j] = 1.0;
        }

    for (int k = 0; k < N; k++)
    {
        temp = A[k][k];

        for (int j = 0; j < N; j++)
        {
            A[k][j] /= temp;
            E[k][j] /= temp;
        }

        for (int i = k + 1; i < N; i++)
        {
            temp = A[i][k];

            for (int j = 0; j < N; j++)
            {
                A[i][j] -= A[k][j] * temp;
                E[i][j] -= E[k][j] * temp;
            }
        }
    }

    for (int k = N - 1; k > 0; k--)
    {
        for (int i = k - 1; i >= 0; i--)
        {
            temp = A[i][k];

            for (int j = 0; j < N; j++)
            {
                A[i][j] -= A[k][j] * temp;
                E[i][j] -= E[k][j] * temp;
            }
        }
    }

    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            A[i][j] = E[i][j];

}

void VectMul(double A[3][3], double B[3], double C[3]) {
    //Умножение матрицы 3х3 на векторЫ

    for(int i=0;i<3;i++){
        C[i]=A[i][0]*B[0]+A[i][1]*B[1]+A[i][2]*B[2];
    }
}

void GEI3toWGS84(const double Ar1[6],const double t, double Ar2[3]){
    //Преобразование Второй Экваториальной в Гринвическую (ТОЛЬКО ПОЛОЖЕНИЕ)
    double cs, ss, s,
            S[3][3], NP[3][3],
            SNP[3][3];

    precessionVSnutation(t, s, NP);
    cs = cos(s);
    ss = sin(s);
    S[0][0]=cs;		S[0][1]=ss; 	S[0][2]=0;
    S[1][0]=-ss;	S[1][1]=cs; 	S[1][2]=0;
    S[2][0]=0;		S[2][1]=0; 	 	S[2][2]=1;
    for(int c=0; c<3; c++)
        for(int i=0; i<3; i++){
            SNP[i][c]=0;
            for(int j=0; j<3; j++) SNP[i][c]+=S[i][j]*NP[j][c];
        }
    for(int i=0; i<3; i++){
        Ar2[i]=0;
        for(int j=0; j<3; j++) Ar2[i]+=SNP[i][j]*Ar1[j];
    }
}

void WGS84toGEI(const double Ar1[6], const double t, double Ar2[6]){
    //Преобразование Гринвической во Вторую Экваториальную

    double cs, ss, s,
            S[3][3], NP[3][3],
            PNS[3][3];

    precessionVSnutation(t,s,NP);
    cs = cos(s);
    ss = sin(s);
    S[0][0]=cs;		S[0][1]=-ss; 	S[0][2]=0;
    S[1][0]=ss;		S[1][1]=cs; 	S[1][2]=0;
    S[2][0]=0;		S[2][1]=0; 	 	S[2][2]=1;
    for(int c=0; c<3; c++)
        for(int i=0; i<3; i++){
            PNS[i][c]=0;
            for(int j=0; j<3; j++) PNS[i][c]+=NP[j][i]*S[j][c];
        }
    for(int i=0; i<3; i++){
        Ar2[i]=0;
        for(int j=0; j<6; j++) Ar2[i]+=PNS[i][j]*Ar1[j];
    }
}

void GEItoWGS84(const double Ar1[6],const double t, double Ar2[6]){
    //Преобразование Второй Экваториальной в Гринвическую

    double cs, ss, s,
           S[3][3], NP[3][3],
           SNP[3][3];

    precessionVSnutation(t, s, NP);
    cs = cos(s);
    ss = sin(s);
    S[0][0]=cs;		S[0][1]=ss; 	S[0][2]=0;
    S[1][0]=-ss;	S[1][1]=cs; 	S[1][2]=0;
    S[2][0]=0;		S[2][1]=0; 	 	S[2][2]=1;
    for(int c=0; c<3; c++)
        for(int i=0; i<3; i++){
            SNP[i][c]=0;
            for(int j=0; j<6; j++) SNP[i][c]+=S[i][j]*NP[j][c];
        }
    for(int i=0; i<3; i++){
        Ar2[i]=0;
        for(int j=0; j<3; j++) Ar2[i]+=SNP[i][j]*Ar1[j];
    }
}

double GMST(double t) {
    // Перевод Юлианского дня в Звезное время
    double st, JD, JD0, Tut, n;

    JD = t;
    modf(JD - 0.5,&n);
    JD0 = n + 0.5;
    Tut = (JD0 - 2451545)/36525;
    st = 1.7533685592 + 628.3319706889*Tut + 6.7707139e-6*Tut*Tut
            - 4.50876*10.0e-10*Tut*Tut*Tut + 1.002737909350795*2*M_PI*(JD - JD0);
    modf(st/(2*M_PI),&n);
    return st-n*2*M_PI; // в приведённых радианах
}

void precessionVSnutation(const double t, double &s, double NP[3][3]){
    //Прецессия и нутация
    double ksi, zeta, teta, psi, eps, eps0;
    double 	c_ksi, s_ksi,
            c_zeta, s_zeta,
            c_teta, s_teta,
            c_psi, s_psi,
            c_eps, s_eps,
            c_eps0, s_eps0;
    double T, T2, T3;
    double Ml,Ms,ul,Ds,Uzl;
    double P[3][3], N[3][3];
    T   = (UTCtoTDB(t) - 2451545.0)/36525.0;
    T2=T*T;
    T3=T2*T;
    ksi  = 0.011180860*T + 1.464E-6*T2 + 8.70E-8*T3;
    zeta = 0.011180860*T + 5.308E-6*T2 + 8.90E-8*T3;
    teta = 0.009717173*T - 2.068E-6*T2 - 2.02E-7*T3;
    eps0 = 0.4090928042 - 0.2269655E-3*T - 2.86E-9*T2 + 8.80E-9*T3;

    Ml   = 2.355548394 + (1325*2*M_PI + 3.470890873)*T + 1.517952E-4*T2 + 3.103E-7*T3;
    Ms   = 6.240035940 +   (99*2*M_PI + 6.266610600)*T - 2.797400E-6*T2 - 5.820E-8*T3;
    ul   = 1.627901934 + (1342*2*M_PI + 1.431476084)*T - 6.427170E-5*T2 + 5.340E-8*T3;
    Ds   = 5.198469514 + (1236*2*M_PI + 5.360106500)*T - 3.340860E-5*T2 + 9.220E-8*T3;
    Uzl  = 2.182438624 -    (5*2*M_PI + 2.341119397)*T + 3.614290E-5*T2 + 3.880E-8*T3;

    eps = 0; psi = 0;
    for(int i=0;i<=105;i++){
        psi += (coef_A[i] + coef_B[i]*T)*sin(coef_a1[i]*Ml + coef_a2[i]*Ms +coef_a3[i]*ul +coef_a4[i]*Ds +coef_a5[i]*Uzl);
        eps += (coef_C[i] + coef_D[i]*T)*cos(coef_a1[i]*Ml + coef_a2[i]*Ms +coef_a3[i]*ul +coef_a4[i]*Ds +coef_a5[i]*Uzl);
    }
    psi  = psi*1E-4/3600./180.*M_PI;
    eps  = eps*1E-4/3600./180.*M_PI;
    eps += eps0;

    c_ksi=cos(ksi);
    s_ksi=sin(ksi);
    c_zeta=cos(zeta);
    s_zeta=sin(zeta);
    c_teta=cos(teta);
    s_teta=sin(teta);
    c_psi=cos(psi);
    s_psi=sin(psi);
    c_eps=cos(eps);
    s_eps=sin(eps);
    c_eps0=cos(eps0);
    s_eps0=sin(eps0);

    //Истинное звездное время TRUE_GMST
    s=GMST(t)+psi*c_eps;

    P[0][0] =  c_ksi*c_zeta*c_teta - s_ksi*s_zeta;
    P[0][1] = -s_ksi*c_zeta*c_teta - c_ksi*s_zeta;
    P[0][2] = -c_zeta*s_teta;

    P[1][0] =  c_ksi*s_zeta*c_teta + s_ksi*c_zeta;
    P[1][1] = -s_ksi*s_zeta*c_teta + c_ksi*c_zeta;
    P[1][2] = -s_zeta*s_teta;

    P[2][0] =  c_ksi*s_teta;
    P[2][1] = -s_ksi*s_teta;
    P[2][2] =  c_teta;

    N[0][0] =  c_psi;
    N[0][1] = -s_psi*c_eps0;
    N[0][2] = -s_psi*s_eps0;

    N[1][0] =  s_psi*c_eps;
    N[1][1] =  c_psi*c_eps*c_eps0+s_eps*s_eps0;
    N[1][2] =  c_psi*c_eps*s_eps0-s_eps*c_eps0;

    N[2][0] =  s_psi*s_eps;
    N[2][1] =  c_psi*s_eps*c_eps0-c_eps*s_eps0;
    N[2][2] =  c_psi*s_eps*s_eps0+c_eps*c_eps0;

    for(int c=0; c<3; c++)
        for(int i=0; i<3; i++){
            NP[i][c]=0;
            for(int j=0; j<3; j++)
                NP[i][c]+=N[i][j]*P[j][c];
        }

}

void MatMul(double A[3][3], double B[3][3], double C[3][3]) {
    //Умножение матрицы 3х3 на матрицу 3х3

    int i, j, k;

    for(i=0; i<3; i++){
        for(j=0; j<3; j++){
            C[i][j] = 0.0;
            for(k=0; k<3; k++){
                C[i][j] += A[i][k]*B[k][j];
            }
        }
    }
}

void GrTurn(double t, double T[3][3]) {
    //Расчет матрицы поворота на угол, пропорциональный времени

    T[0][0] = cos(w_earth*t);    T[0][1] = -sin(w_earth*t);   T[0][2] = 0.0;
    T[1][0] = sin(w_earth*t);    T[1][1] = cos(w_earth*t);    T[1][2] = 0.0;
    T[2][0] = 0.0;               T[2][1] = 0.0;               T[2][2] = 1.0;
}

void X6Asn2GrF(double t, double rV[6], double rVG[6]) {
    //Перевод вектора состояния из АСН в Гринвич, замороженный на КП

    double T[3][3], r[3], V[3], rG[3], VG[3];
    int i;

    for(i=0; i<3; i++){
        r[i] = rV[i];
        V[i] = rV[i+3];
    }

    GrTurn(t, T);

    VectMul(T, r, rG);
    VectMul(T, V, VG);

    VG[0] = VG[0]-rG[1]*w_earth;
    VG[1] = VG[1]+rG[0]*w_earth;

    for(i=0; i<3; i++){
        rVG[i] = rG[i];
        rVG[i+3] = VG[i];
    }
}

void Mst2GrF(double t, double fi, double lam, double psi, double rV[6], double rVG[6]) {
    //Перевод вектора состояния из ННСК в ГрКП

    //t - время от КП до ОТП
    double sf, cf, sl, cl, sp, cp;
    double T[3][3], L[3][3], TL[3][3], r[3], V[3], rG[3], VG[3];
    int i;

    for(i=0; i<3; i++){
        r[i] = rV[i];
        V[i] = rV[i+3];
    }

    GrTurn(t, T);

    sf=sin(fi);  cf=cos(fi);
    sl=sin(lam); cl=cos(lam);
    sp=sin(psi); cp=cos(psi);

    L[0][0] = -sf*cp*cl-sp*sl;   L[0][1] = cf*cl;    L[0][2] = sf*sp*cl-cp*sl;
    L[1][0] = -sf*cp*sl+sp*cl;   L[1][1] = cf*sl;    L[1][2] = sf*sp*sl+cp*cl;
    L[2][0] = cf*cp;             L[2][1] = sf;       L[2][2] = -cf*sp;

    MatMul(T, L, TL);

    VectMul(TL, r, rG);
    VectMul(TL, V, VG);

    for(i=0; i<3; i++){
        rVG[i] = rG[i];
        rVG[i+3] = VG[i];
    }
}
