#ifndef SPACEPORTWINDOW_H
#define SPACEPORTWINDOW_H

#include <QWidget>

namespace Ui {
class SpaceportWindow;
}

class SpaceportWindow : public QWidget {

    Q_OBJECT

public:

    explicit SpaceportWindow(QWidget *parent = 0);
    ~SpaceportWindow();

private slots:

    void on_list_widget_itemPressed();
    void on_button_add_clicked();
    void on_button_del_clicked();
    void on_button_up_clicked();
    void on_button_down_clicked();

    void on_edits_changed();

private:

    Ui::SpaceportWindow *ui;

    int n;
    QString prev_item;
    void load_spaceports();
    void select_spaceport();
};

#endif // SPACEPORTWINDOW_H
