#ifndef SEPARATIONWINDOW_H
#define SEPARATIONWINDOW_H

#include <QWidget>

namespace Ui {
class SeparationWindow;
}

class SeparationWindow : public QWidget
{
    Q_OBJECT

public:
    explicit SeparationWindow(QWidget *parent = 0);
    ~SeparationWindow();

private:
    Ui::SeparationWindow *ui;
};

#endif // SEPARATIONWINDOW_H
