#include "orbit.h"

#include "math.h"
#include "iostream"
#include "conversions.h"
#include "constants.h"

Orbit::Orbit() {

    t_tmi = 0.0;
    t_ref = 0.0;

    period_tmi = 0.0;
    period_ref = 0.0;
    period_tol = 0.0;
    period_dif = 0.0;

    for (int i = 0; i < 9; i++) {
        elem_tmi[i] = 0.0;
        elem_ref[i] = 0.0;
        elem_tol[i] = 0.0;
        elem_dif[i] = 0.0;
    }
}

Orbit::Orbit(QString orbit_name): orbit_name(orbit_name) {

    t_tmi = 0.0;
    t_ref = 0.0;

    period_tmi = 0.0;
    period_ref = 0.0;
    period_tol = 0.0;
    period_dif = 0.0;

    for (int i = 0; i < 9; i++) {
        elem_tmi[i] = 0.0;
        elem_ref[i] = 0.0;
        elem_tol[i] = 0.0;
        elem_dif[i] = 0.0;
    }
}

Orbit::Orbit(QString orbit_name, QString scraft_name): orbit_name(orbit_name), scraft_name(scraft_name) {

    t_tmi = 0.0;
    t_ref = 0.0;

    period_tmi = 0.0;
    period_ref = 0.0;
    period_tol = 0.0;
    period_dif = 0.0;

    for (int i = 0; i < 9; i++) {
        elem_tmi[i] = 0.0;
        elem_ref[i] = 0.0;
        elem_tol[i] = 0.0;
        elem_dif[i] = 0.0;
    }
}

void Orbit::calc_dif() {

    period_dif = period_tmi - period_ref;

    if(period_tol == 0.0) {
        tol_flag.append("");
    } else if(period_dif < period_tol){
        tol_flag.append("В допуске");
    } else {
        tol_flag.append("Вне допуска!");
    }

    for(int i = 0; i < 9; i++) {

        elem_dif[i] = elem_tmi[i] - elem_ref[i];
        if(elem_tol[i] == 0.0) {
            tol_flag.append("");
        } else if (fabs(elem_dif[i]) < elem_tol[i]) {
            tol_flag.append("В допуске");
        } else {
            tol_flag.append("Вне допуска!");
        }
    }
}
