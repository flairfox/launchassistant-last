#ifndef EDITWINDOW_H
#define EDITWINDOW_H

#include <QWidget>

#include "orbit.h"

namespace Ui {

class EditWindow;

}

class EditWindow : public QWidget {

    Q_OBJECT

public:

    explicit EditWindow(QWidget *parent = 0);
    ~EditWindow();

public slots:

    void recieve_orbit(Orbit orbit);

    void recieve_request();

signals:

    void send_edited(Orbit orbit);

    void send_new(Orbit orbit);

private slots:
    void on_button_ok_clicked();

    void on_button_cancel_clicked();

private:

    Ui::EditWindow *ui;

    int flag;
};

#endif // EDITWINDOW_H
