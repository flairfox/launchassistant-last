﻿#ifndef uTransH
#define uTransH

#include <QDateTime>
#include <QTime>

typedef double (*simple_func)(double var_prm);

double UTCtoTDB(double JD);
double TDBtoUTC(double JD);
double TimeToSeconds(QTime t);
QTime SecondsToTime(double t);
double ToJulianDate(QDateTime QDT);
double SetJulianDate(int Year, int Month, int Day, int Hour, int Min, double Sec);
double ModulVec (double x[3]);
void ElemToVect(const double Ar1[9], double Ar2[6]);
void VectToElem(const double Ar1[6], double Ar2[9]);
void MatInv(double A[3][3], int N);
void VectMul(double A[3][3], double B[3], double C[3]);
void GEI3toWGS84(const double Ar1[6], const double t, double Ar2[3]);
void GEItoWGS84(const double Ar1[6], const double t, double Ar2[6]);
void WGS84toGEI(const double Ar1[6], const double t, double Ar2[6]);
void precessionVSnutation(const double t, double &s, double NP[3][3]);
void MatMul(double A[3][3], double B[3][3], double C[3][3]);
void GrTurn(double t, double T[3][3]);
void X6Asn2GrF(double t, double rV[6], double rVG[6]);
void Mst2GrF(double t, double fi, double lam, double psi, double rV[6], double rVG[6]);

#endif
