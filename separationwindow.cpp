#include "separationwindow.h"
#include "ui_separationwindow.h"

SeparationWindow::SeparationWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SeparationWindow)
{
    ui->setupUi(this);
}

SeparationWindow::~SeparationWindow()
{
    delete ui;
}
