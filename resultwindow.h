#ifndef RESULTWINDOW_H
#define RESULTWINDOW_H

#include <QWidget>

#include "orbit.h"

namespace Ui {
class ResultWindow;
}

class ResultWindow : public QWidget
{
    Q_OBJECT

public:

    explicit ResultWindow(QWidget *parent = 0);
    ~ResultWindow();

public slots:

    void recieve_orbit(Orbit orbit);
    void result_clear();
    void result_show();


private slots:

    void on_print_clicked();
    void on_pushButton_clicked();
    void on_toolButton_clicked();
    void on_rbutton_GrKP_clicked();
    void on_rbutton_J2000_clicked();

private:

    Ui::ResultWindow *ui;

    QString fill_html (Orbit orbit);
    void addTabs(Orbit orbit);
    void draw_tabs();

};

#endif // RESULTWINDOW_H
